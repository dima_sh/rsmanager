package ru.rsmanager.test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import ru.sbtsbol.admin.rsmanager.error.RsManagerException;
import ru.sbtsbol.admin.rsmanager.model.Resource;
import ru.sbtsbol.admin.rsmanager.service.FileResourceManagerImpl;
import ru.sbtsbol.admin.rsmanager.service.ResourceManager;



public class RsFileManagerTest {
	
   private static String resourceManagerName1 = "resourceManagerName1";
   private static String resourceManagerPath1 = "resourceManagerPath1";
   private static ResourceManager manager1;
   private static String realManager1Path = "";
   private static byte[] fileContent = new byte[]{1,2,3,3,2,1};
   
	
   @BeforeClass
   public static void init() throws IOException {
	   String tmpRootName = System.getProperty("java.io.tmpdir");
	   realManager1Path = tmpRootName +  resourceManagerPath1 ;
	   tryToCreateTmpDir(realManager1Path);
	   System.out.println("The real path is:" + realManager1Path);
	   FileResourceManagerImpl resourceManager = new FileResourceManagerImpl(); 
	   resourceManager.setRootPath(realManager1Path);
	   resourceManager.setRootName(resourceManagerName1);
	   manager1 = resourceManager; 
   }
   
   @AfterClass
   public static void destroyTmpDirs() throws IOException {
	   softDeleteTmpDir(new File(realManager1Path));
   }
   
   @Test
   public void testCreateDir() throws RsManagerException {
	   Resource createdDir = manager1.mkDir(resourceManagerName1 + "\\" + "Test Dir"); 
	   Resource foundedDir = manager1.findResource(createdDir.getAbsolutePath());
	   Assert.assertEquals("Error creating dir", createdDir, foundedDir);
   }
   @Test
   public void testCreateFile() throws RsManagerException, IOException {
	   Resource createdDir = manager1.mkDir(resourceManagerName1 + "/" + "Test Dir1");
	   String createdFilePath = createdDir.getAbsolutePath() + File.separator + "createfile.txt";
	   Resource expectedFile = manager1.createFile(fileContent, createdFilePath);
	   Resource actualFile = manager1.findResource(expectedFile.getAbsolutePath());
	   InputStream is = manager1.getInputStream(actualFile.getAbsolutePath());
	   byte[] actualFileContent = new byte[fileContent.length];
	   try {
	     IOUtils.read(is, actualFileContent);
	   } finally {
	     IOUtils.closeQuietly(is);
	   }
	   
	   Assert.assertEquals("Error creating file [defferent name]", expectedFile.getId(), actualFile.getId());
	   Assert.assertTrue("Error creating file [defferent content]", Arrays.equals(actualFileContent, fileContent));
   }
   
   
   public static void tryToCreateTmpDir(String tmpDir) throws IOException {
	   File file = new File(tmpDir);
	   
	   softDeleteTmpDir(file); 
		 
	   boolean isMaked = file.mkdir();
	   if (!isMaked) {
		   Assert.fail("Can't make tmp target " + tmpDir + ". May no rights!");
	   }
   }
   
   public static void softDeleteTmpDir(File tmpDir) throws IOException {
		  FileUtils.deleteDirectory(tmpDir);
   }
	
}
