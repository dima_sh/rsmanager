﻿<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false"
	trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" type="image/x-icon" href="/favicon.ico" />
<title>Backbone filemanager</title>
<link rel="stylesheet/less" href="./styles/main.less" />
<script src="./js/libs/utils/less.js"></script>
</head>
<body>

	<script data-main="./js/init" src="./js/libs/require/require.js"></script>

	<script type="text/javascript">
		require.config({
			//    Поключить зависимости. init  - хранит pathes базовых libs, Views, Contollers, etc...
			deps : [ "./init", "app/modules/rsmanager/application" ]
		});
	</script>
    <center>
	<div id="regionContent"></div>
	</center>
	<div id="modal-container" tabindex="-1" role="dialog"
		aria-hidden="true"></div>
	<div id="loader-container" tabindex="-1" role="dialog"
		aria-hidden="true"></div>
</body>
</html>
