require([
	'jquery',
	'backbone',
	'app/application',
	'app/modules/rsmanager/Router',
	
],
function ($, Backbone, App, RSManagerRouter) {
	'use strict';
	
	
	 App.addInitializer(function(options) {
		  if (Backbone.history) Backbone.history.start();
		
	 });
	 
	 // return size string for info about resource
	 App.calculateSize = function(size) {
 		if (size == 0 ){
			return ' 0 MB';
		}
		size = (size/1024).toPrecision(3);
		if (size < 100) {
			return size + ' KB';
		} else {
			return (size/1024).toPrecision(3) + ' MB';
		}
	 },
	
	
	 
	 App.start();
    
});
