define([
	'underscore',
	'backbone',
	'app/application',
	'app/modules/rsmanager/rsconfig',
],
function(_, Backbone, App, config) {
	'use strict';
	/* Return a model class definition */
	return Backbone.Model.extend({	
		url: '/resource/',
		methodUrl: {
			'delete': function() {
				return '/resource/'+ this.get('absolutePath').replace(/\\/g,'%5C');
			},
			'update': function() {
				return '/resource/'+ this.get('absolutePath').replace(/\\/g,'%5C');
			}
     
		},
		sync: function(method, model, options) {
		    if (model.methodUrl && model.methodUrl[method.toLowerCase()]) {
		      options = options || {};
		      options.url = model.methodUrl[method.toLowerCase()].call(this);
		    }
		    Backbone.sync(method, model, options);
		 },
		 
		defaults: {
			mimeClass : function() {
				return this.mimeTypes.replace('/', '-');
			},
			tmbPath: function() {
				return this.absolutePath.replace(/\\/g,'%5C');
			},
			normalizedPath: function() {
				return this.absolutePath.replace(/\\/g,'%5C');
			}
		},
		initialize: function() {
			this.selected = false;
			if (this.get('absolutePath'))
			   this.set('absolutePath', this.get('absolutePath').replace(/\\\\/g,'\\'));
		},
		/**
		 * Set flag that model selected
		 * @multiselect if true then multiselect is on
		 */
		select: function(multiselect) {
			this.selected = true;
			this.trigger('select', this);
			if (!multiselect) {
				var me = this;
				_(this.collection.models).each(function(model){
	                if (model.get('id')!= me.get('id')) {
	                	model.unselect();
	                }				
				});
			}
		},
		// unselect model
		unselect: function() {
			if (this.isSelected()) {
				this.selected = false;
				this.trigger('unselect', this);
			}
		},
		// check that model is selected
		isSelected: function() {
			return this.selected == true; 
		},
		// save flag on cut/paste operation
		setOperation: function(operation) {
			this.set({'operation': operation});
		},
		// clear operation state
		unsetOperation: function() {
			this.unset('operation');
		},
		// check that parent volume is search volume
		isSearchTag: function() {
			return config.isSearchTag(this.get('volumeId'));
		}
		
    });
});