define([
	'backbone',
	'underscore',
	'backbone.marionette',
	'app/application',
	'hbs!tmpl/rsmanager/item/resourceinfo',
	'hbs!tmpl/rsmanager/item/resourcesinfo'
],
function(Backbone, _, Marionette, App, ResInfo, ResInfos) {
	'use strict';
	/* Return a Info ItemView class definition in footer panel */
	return Marionette.ItemView.extend({
		template: ResInfo,
		className: 'b-popup',

		events: {
			'click ._ok': 'ok',
		},
		
		initialize: function() {
			if (this.model.get('total')) {
				this.template = ResInfos; 
			}
		},
	    
	    ok: function() {
		   this.close();
	    },
	    

	});
});