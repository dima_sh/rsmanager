define([
	'backbone.marionette',
	'app/application',
	'app/modules/rsmanager/rsconfig',
	'hbs!tmpl/rsmanager/layout/rsaside',
	'app/modules/rsmanager/views/composite/Volume',
	'app/modules/rsmanager/collections/Volume',
	'app/modules/rsmanager/models/Volume'
],
/**
 * @author SBT-Shiryaev-DmV
 */
function(Marionette, App, config, Template, VolumeView, Volumes, Volume ) {
    'use strict';
	/* Return a Left Panel Layout class definition */
	return Marionette.Layout.extend ({
		className:'finder_aside',
		template: Template,
		
		regions:  {
			volumeregion: 'div.finder_row',
		},
		
		options: {
    		height:900, 
    		off: []
    	},
    
    	initialize: function() {
    		this.listenTo(App.vent, 'rsmanager:navtree', this.navigate);
    		this.listenTo(App.vent, 'rsmanager:search', this.search);
    		this.volumes = new Volumes();
    	},
    	// COMMAND SEARCH PREPARE
    	search: function(mask) {
    		var model = new Volume({type:'search', id:'s_'+ mask, name: 'search result: ('+ mask + ')', searchString: mask});
    		this.volumes.add(model);
    		model.trigger('selectMe');
    	},
	    
	    onRender: function() {
	    	this.$el.css('height', config.options.height);
	    	this.volumeregion.show(new VolumeView({collection: this.volumes}));
	    	//COMMAND VOLUMES
	    	this.volumes.fetch({async: false});
	    },
    	/* select first volume */
    	select: function() {
    		if (localStorage.getItem('rsmanager:historypath') != undefined) {
    			this.navigate(localStorage.getItem('rsmanager:historypath'));
    		} else if (this.volumes)
    		  this.volumes.first().trigger('selectMe');
    	},
	    // navigate in tree by path 
	    navigate: function(path) {
	    	var paths = path.split('\\');
	    	var volume = this.volumes.findWhere({name: paths.shift()});
	    	if (volume) {
	    		volume.trigger('navigateMe', paths);
	    	}
	    }
    	
    	
	});

});
