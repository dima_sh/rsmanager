define([ 
    'backbone',
    'underscore',
	'backbone.marionette',
	'app/application',
	'app/modules/rsmanager/rsconfig',
	'app/modules/rsmanager/collections/Resource'
],
function(Backbone,_ , Marionette, App, config, Resources) {
	'use strict';
	/* Return a Behaviour that manage upload operation */
	return Marionette.Behavior.extend({
		
		ui: {
			fileinput: 'form#uploader input[type="file"]',
			uploadButton: '._upload',
		},
		
		events: {
	        'change form#uploader' : 'addFile',
	        'click @ui.uploadButton' : 'clickUpload'
		},
		
		initialize: function(options) {
			this.historyPointer = 0;
			this.listenTo(App.vent,'tree:select', this.onSelectDirectory);
		},
		
		onSelectDirectory: function(resources, currdir) {
    		this.currdir = currdir;
    		this.resources = resources;
    	},
		
		clickUpload : function() {
	        this.ui.fileinput.click();
	    },
	
     	addFile: function(evt) {
              evt.stopPropagation;
              evt.preventDefault;
              var files = evt.target.files;
              if (this.currdir == undefined) {
      			return false;
      		  }
              this.uploadFiles(files, this.currdir);
         },
	    
	    
	    /* upload file action */ 
	    uploadFiles: function(files, parent) {
    		var data = new FormData();
    		var me = this;
    		$.each(files, function(key, value) {
    			data.append('upload[]', value);
    		});
    		
    		
    		var xhr = new XMLHttpRequest();
    		xhr.upload.addEventListener('progress', function(evt){
    			
    	    	if (evt.lengthComputable) {
    	    		var percentComplete = Math.round(evt.loaded * 100/ evt.total);
    	    		me.setProgress(percentComplete);
    	    	}
    		}, false);
    		xhr.addEventListener('loadstart', me.view.setBuzy(me.ui.uploadButton)),
    		xhr.addEventListener('loadend', me.view.setReady(me.ui.uploadButton)),
    		xhr.addEventListener('error', function(a,b,c){
    			me.view.setReady(me.ui.uploadButton);
    		}),
    		xhr.addEventListener('load', function(evt) {
    			if (me.view.ui.uploadProgress)
    				me.view.ui.uploadProgress.html('');
    			if (evt.target.status != 200)
    			   App.vent.trigger('rsmanager:error', [new Error({code: evt.target.statusText, message: evt.target.responseText})]);
    			else
    			   App.vent.trigger('file:added', new Resources(JSON.parse(evt.target.response), {volumeId: parent.get('volumeId') }));
    		});
    		//COMMAND UPLOAD
    		xhr.open('POST', App.baseUrl +'/resource/upload/' + (parent.get('absolutePath') == "" ? parent.get('name') : parent.get('absolutePath')));
    		xhr.send(data);
	    },
	    
	    setProgress: function(percent) {
	    	if (!(this.view.ui.uploadProgress).find('progress').length) {
	    		this.view.ui.uploadProgress.append('<progress class="btn-set_item-upload-progress" max="100" value="0"/>');
           	} 
	    	var $progress = (this.view.ui.uploadProgress).find('progress');
	    	$progress.attr('value', percent);	
	    	
	    },
		
	});
});
