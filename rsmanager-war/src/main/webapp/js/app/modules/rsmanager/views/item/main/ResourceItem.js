define([
	'jquery',
	'backbone',
	'underscore',
	'backbone.marionette',
	'app/application',
	'hbs!tmpl/rsmanager/item/resourceitem',
	'app/modules/rsmanager/collections/Resource'
	//'app/modules/rsmanager/views/composite/Resource'
],
function($, Backbone,_, Marionette, App, Template,  Resource) {
	'use strict';
	
	/* Return a ItemView class definition */
	return Marionette.ItemView.extend({
		template: Template,
	
		className: 'finder_col',
		
		events: {
			'click div': 'onClick',
			'contextmenu div': 'onClick',
			//'mouseover div' : 'onMouseOver',
			'dblclick div': 'dblClick',
			'keydown input': 'onKeydown'
			
		},
		ui: {
			item: 'div.b-file-system-item',
			input: '.file-system-item_name-input',
			name: 'span'
		},
		
		constants: {
			activateCLass: '-active'
		},
		
		initialize: function() {
			this.listenTo(this.model,'select', this._select);
			this.listenTo(this.model,'unselect', this._unselect);
			this.listenTo(this.model,'rename', this.renamePrepare);
			this.listenTo(this.model,'cut', this.cutPrepare);
			this.listenTo(this.model,'uncut', this.cutRemove);
			this.listenTo(this.model,'change:id', this.render);
			if (this.model.isNew()) {
				this.listenTo(App.vent,'rsmngr:clickesc', this.onEscape);
				this.listenTo(App.vent,'rsmngr:msclick', this.onClickOutSide);
			}
		},
        // change ui to show rename operation
		renamePrepare: function() {
			this.ui.input.toggle();
			this.ui.name.toggle();
			this.ui.input.val(this.model.get('name'));
			this.listenTo(App.vent,'rsmngr:clickesc', this.onEscape);
			this.$el.find('input').focus();
		},
		// mark that resource will be cut
		cutPrepare: function() {
			this.ui.item.addClass('-cut');
		},
		// remove marker cut
		cutRemove: function() {
			this.ui.item.removeClass('-cut');
		},
		
		// handle enter on rename or new resource
		onKeydown: function(evt) {
			if (evt.keyCode == 13) {
				evt.preventDefault();
				if (this.model.isNew()) {
				  this.saveModel();
				} else {
				  this.updateModel();
				}  
			}
			
		},
		// handle escape operation
		onEscape: function() {
			if (this.model.isNew()) {
				this.model.destroy();
				this.close();
			} else {
				this.ui.input.toggle();
				this.ui.name.toggle();
				App.vent.off('rsmngr:clickesc');
			}
		},
		//catch event when somwhere clicked esc button
		onClickOutSide: function() {
			if (this.model.isNew()) {
			   this.saveModel();
			} else {
			   this.updateModel();
			}
		},
		// updating model logic
		updateModel: function() {
			var me = this;
			this.model.set({newname: this.ui.input.val()});
			//COMMAND UPDATE(RENAME)
			this.model.save([],{
				wait:true,
				async: false,
				showglobalerr: false,
				error: function(model, response) {
					me.ui.input.toggle();
					me.ui.name.toggle();
					me.model.unset('newname');
					App.vent.off('rsmngr:clickesc');  
					App.vent.off('rsmngr:msclick');
					App.vent.trigger('rsmanager:error', response.responseJSON);
					
				},
			    success: function(model) {
				   App.vent.off('rsmngr:clickesc');  
				   App.vent.off('rsmngr:msclick');
			    }  
			});
		},
		// save model logic
		saveModel: function() {
			var me = this;
			if (this.model.isNew()){
				this.model.set({name: this.ui.input.val()});
				//COMMAND CREATE
				this.model.save([],{
					wait:true,
					async: false,
					showglobalerr: false,
					error: function(model, response) {
						App.vent.trigger('rsmanager:error', response.responseJSON);
						me.model.destroy();
						me.close();
					},
				    success: function(model) {
					   App.vent.off('rsmngr:clickesc');  
					   App.vent.off('rsmngr:msclick');
				    }
				});
			}
		},
		
		onRender: function() {
			if (this.model.isNew()) {
				
				this.ui.input.toggle();
				this.ui.name.toggle();
				this.model.select();
			}
			this.el.addEventListener("contextmenu", function(e){
    			e.preventDefault();
    			App.vent.trigger('rsmanager:contextmenu', e);
    		});
		},
		
		onShow: function() {
			if (this.model.isNew()) {
			  this.$el.find('input').focus();
		    }
		},
		// dblckick event, if directory open children otherwise download file
		dblClick: function(evt) {
			evt.stopPropagation();
	    	evt.preventDefault();
	    	if (this.model.get('mimeTypes') == 'directory') {
	    		if (this.model.isSearchTag()) {
	    			App.vent.trigger('rsmanager:navtree', this.model.get('absolutePath'));	
	    		} else {
	    			this.model.trigger('openEvent', this.model);	
	    		}
	    	} else {
	    		App.vent.trigger('rsmanager:clickfile', this.model);
	    	}
		},
		
		onClose: function() {
			this.model.unselect();
		},
		// by the click select resource or unselect
	    onClick: function(evt) {
	    	evt.stopPropagation();
	    	evt.preventDefault();
	    	var multiselect = false;
	    	if (evt.button == 2 || evt.wich == 3) {
	    		this.model.select(false);
	    		return;
	    	}
	    	if (evt.ctrlKey) {
	    		multiselect = true;
	    	}
	    	if (!this.model.isSelected()) {
	    		this.model.select(multiselect);
	    	} else {
	    		this.model.unselect();
	    	}
	    },
		
		_select: function() {
			this.ui.item.addClass(this.constants.activateCLass);
		},
		
		_unselect: function() {
			this.ui.item.removeClass(this.constants.activateCLass);
		}
	});
});
