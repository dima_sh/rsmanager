define([ 
    'backbone',
    'underscore',
	'backbone.marionette',
	'app/application',
	'app/modules/rsmanager/rsconfig',
	'app/modules/rsmanager/views/item/main/ResourceItem',
	'hbs!tmpl/rsmanager/composite/resourceview'
	
],
function(Backbone,_ , Marionette, App, config, ResourceItem, Template) {
	'use strict';
	//debugger;
	/* Return a CompositeView class definition of Resource View in main Window */
	return Marionette.CompositeView.extend({
		template: Template,

		className: 'finder_row',
		
		itemView: ResourceItem,
		
		initialize: function() {
			this.collection.on('sync', this.render, this);
		},
	    // extend Marionete view method. Filter to show resources that set in mimeOn properties
		addItemView: function (item, ItemView, index) {
			var filter = false;
			if (config.options.mimeOn){
				_(config.options.mimeOn).each(function(mask){
					var mime = item.get('mimeTypes');
					if (mime.match(mask)) {
						filter = true;
					}
				});
			 } else {
				 filter = true;
			 } 
			 if (filter == true) {
				 Backbone.Marionette.CompositeView.prototype.addItemView.apply(this,arguments);	 
			 }
		}, 
	    // extend Marionete view method,add created resource in the begin of resource list
	    appendHtml: function(collectionView, itemView, index) {
	    	 if (itemView.model.isNew()) {
	    		 if (itemView.model.isNew()) {
	    			 var $container = this.getItemViewContainer(collectionView);
	    		     $container.prepend(itemView.el);
	    		 }
	    	 } else {
	    		 Backbone.Marionette.CompositeView.prototype.appendHtml.apply(this,arguments);	 
	    	 }
	    	 	 
	    }
	
	});
});