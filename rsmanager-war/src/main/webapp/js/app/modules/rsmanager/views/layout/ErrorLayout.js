define([
	'backbone.marionette',
	'app/application',
	'hbs!tmpl/rsmanager/layout/rserrorlayout',
	'app/modules/rsmanager/views/composite/info/ErrorView',
	'app/modules/rsmanager/collections/Error',
	'app/modules/rsmanager/models/Error'
],
function(Marionette, App, Template, ErrorView, ErrorCollection, Error ) {
    'use strict';
	/* Return a Show error Layout class definition */
	return Marionette.Layout.extend ({
		
		template: Template,
		className: 'b-error-box',
		
		events: {
			'click .error-box_close-icon': 'clickClose'
		},
		
		regions:  {
			errorcontainer: '.b-error-container'
		},
		
		initialize: function() {
			this.listenTo(App.vent,'rsmanager:error', this.handleError);
		},
		/* handle some error */
		handleError: function(error) {
			if (Array.isArray(error)) {
				this.errorCollection = new ErrorCollection(error, {parse:true});
			} else {
				this.errorCollection = new ErrorCollection([error], {parse:true});
			}
			this.errorcontainer.show(new ErrorView({collection:this.errorCollection}));
			
			if (!this.$el.is(':visible'))
				   this.$el.slideToggle(400);
		},
		
		onRender: function() {
			this.$el.css('display', 'none');
		},
		
		clickClose: function(evt) {
			evt.preventDefault;
			this.errorcontainer.close();
			this.$el.slideToggle(400);
		}
    	
	});

});