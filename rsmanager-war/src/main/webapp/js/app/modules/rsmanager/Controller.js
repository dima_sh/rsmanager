define([
	'underscore',
	'backbone',
	'backbone.marionette',
	'app/application',
	'app/modules/rsmanager/views/layout/RSManagerLayout',
	
],
function(_, Backbone, Marionette, App , RSLayout){
	'use strict';
	//return Controller of RSManager
	return Marionette.Controller.extend({
			
		showRsManager: function() {
			App.layout = new RSLayout();
			App.layout.render();
		}
		
	
	});
});