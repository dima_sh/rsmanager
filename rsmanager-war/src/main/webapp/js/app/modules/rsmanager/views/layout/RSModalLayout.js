define([
    'jquery',
	'backbone',
	'backbone.marionette',
	'app/application',
	'hbs!tmpl/rsmanager/rsmodal',
	'app/modules/rsmanager/views/layout/RSWindowLayout'
],
function($, Backbone, Marionette, App, ModalView, RsView) {
	'use strict';
	/* Return a Layout class definition to show RSManager in modal wondow */
	return Marionette.Layout.extend({
		template: ModalView,
		
		className: 'modal',
		
		events: {
			'click .btn-primary': 'ok',
			'click .close': 'cancel',
			'click .btn-default': 'cancel'
		},
		
		regions: {
           rsmanager:'.rsmanager'			
		},
		
		options: {
			width:770,
			height:500,
			off:['rename', 'paste', 'cut', 'download', 'paste', 'delete', 'copy', 'createDir', 'upload']
		},
		
		
		initialize: function(options) {
		  this.options = _.defaults(options || {}, this.options);	
		  this.listenTo(App.vent,'rsmanager:clickfile', this.clickFile);
		  this.setUrl = options.setUrl;
		},
		
		onShow: function() {
			this.rsView = new RsView(this.options);
			this.rsmanager.show(this.rsView);
			
		},
		// catch events to click mouse on the file to get resource url		
		clickFile: function(model) {
			this.setUrl('../resource/get/' + model.get('volumeId') + '/' + model.get('absolutePath'));
			this.close();
		},
		
		onClose: function() {
			App.vent.off('rsmngr:clickfile', this.clickFile, this);
		},
		
	    
	    ok: function() {
	    	this.close();
	    },
	    
	    cancel: function() {
	    	this.close();
	    }
		
	
	});
});