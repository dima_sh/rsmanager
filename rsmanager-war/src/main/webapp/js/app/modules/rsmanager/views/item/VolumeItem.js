define([
	'jquery',
	'backbone',
	'underscore',
	'backbone.marionette',
	'app/application',
	'hbs!tmpl/rsmanager/item/volumetreeitem',
	'app/modules/rsmanager/collections/Resource',
	'app/modules/rsmanager/views/composite/Resource',
	'app/modules/rsmanager/views/item/AbstractTreeItem'
	
	
],
function($, Backbone, _, Marionette, App, Template, Resource, ResourceView, TreeItem) {
	'use strict';
	
	/* Return a ItemView class definition of Volume tree */
	return TreeItem.extend({
		template: Template,
		tagName: 'li',
		className: 'folder-tree_list-item',
		
		ui: {
			loader: 'div div.folder-tree_list-item-folder-loader',
			itemEl : '.folder-tree_list-item-folder',
			contentDiv: '.folder-tree_list-item-folder'
			
		},
		
		events : {
			'click .folder-tree_list-item-folder-trigger-icon' : 'clickArrow',
			'click .folder-tree_list-item-folder' : 'clickSelect'
		},
		
		props: {
			openClass : '-opened',
			selectClass: '-active'
		},
		
		initialize: function() {
			this.$el.addClass("_" + this.model.get('type'));
			this.model.set({volumeId: this.model.get('id')});
			this.listenTo(App.vent,'tree:select', this._unselect);
			this.listenTo(this.model, 'selectMe', this.selectMe);
			this.listenTo(this.model, 'navigateMe', this.navigateMe);
		},
		
		_open: function() {
			this.Resourcecollection = new Resource([],{volumeId: this.model.get('id'), 
				parent: this.model.isSearchTag()? this.model.get('id'):this.model.get('name')});
			this.showLoader();
			this.Resourcecollection.fetch({
				loader: this, showglobalerr: false, async:false,
				error: function(models, response) {
					App.vent.trigger('rsmanager:error', response);
				}});
			//this.listenTo(this.Resourcecollection,'sync', this.load);
			this.listenTo(this.Resourcecollection,'openEvent', this.openChild);
			this.listenTo(this.Resourcecollection,'reload', this.reload);
			this.load();
		},
	
		load: function() {
			if (!this.model.isSearchTag()){
				this.Resourceview = new ResourceView({collection:this.Resourcecollection});
				this.$subtree = this.$el.append('<ul class="folder-tree_list"></ul>').find('ul');
				if (!this.isOpen()) {
					this.$subtree.hide();
				}
				this.Resourceview.setElement(this.$subtree).render();
				
			}
		}
		
		
	
	});
});
