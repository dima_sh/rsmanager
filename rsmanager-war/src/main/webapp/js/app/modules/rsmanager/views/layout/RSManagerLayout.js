define([
	'backbone.marionette',
	'app/application',
	'hbs!tmpl/rsmanager/layout/rslayout',
	'app/modules/rsmanager/views/layout/RSWindowLayout'
],
function(Marionette, App, LayoutTmpl, FView ) {
    'use strict';
	/* Return a Root Layout class definition */
	return Marionette.Layout.extend ({
		template: LayoutTmpl,
    	
		el: '#regionContent',
    	/* Layout sub regions */
    	regions: {
    		finderHolder: 'div.rsmanager'
    	},
	    
	    onRender: function() {
	    	this.finderHolder.show(new FView());
	    }
    	
	});

});
