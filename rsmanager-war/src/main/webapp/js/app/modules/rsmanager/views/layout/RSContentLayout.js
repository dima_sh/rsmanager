define([
	'backbone.marionette',
	'app/application',
	'app/modules/rsmanager/rsconfig',
	'hbs!tmpl/rsmanager/layout/rsmain',
	'app/modules/rsmanager/views/composite/main/Resource',
	'app/modules/rsmanager/views/layout/ErrorLayout',
	'app/modules/rsmanager/models/Resource',
],
/**
 * @author Dmitry_Shiryaev
 * 
 */
function(Marionette, App, config, Template, ResourceView, ErrorPanel, Resource) {
    'use strict';
	/* Return a Content Layout class definition */
	return Marionette.Layout.extend ({
		className:'finder_base',
		template: Template,
    
    	regions: {
    		content : '.finder_main',
    		ferror: '.finder_error-box'
    		
    	},
    	
    	initialize: function() {
    		this.listenTo(App.vent,'file:added', this.addFiles);
    		this.listenTo(App.vent,'dir:create', this.createDir);
    		this.ErrorPanel = new ErrorPanel();
    	},
    	// prepare to create dir
    	createDir: function() {
    		var dirname = 'new folder';
    		var rsname = dirname;
    		var i = '';
    		while (this.collection.findWhere({name: rsname})) {
    			i++;
    			rsname = dirname + ' '+ i;
			}
    		var model = new Resource({mimeTypes: 'directory', name: rsname});
    		this.collection.add(model);
    	},
		//set files events, show on navigator
    	setFiles: function(files) {
    		this.collection = files;
    		this.content.close();
    		this.content.show(new ResourceView({collection:files}));
    	},
    	//add new files eveent need to add in content menu
    	addFiles: function(files) {
    		this.collection.add(files.models, {merge: true});
    	},
    	
    	onRender: function() {
    		this.ferror.show(this.ErrorPanel);
    		this.el.addEventListener("contextmenu", function(e){
    			e.preventDefault();
    			App.vent.trigger('rsmanager:contextmenu', e);
    		});
    	},
    	
    	onShow: function() {
    		Backbone.$(this.$el).find(this.regions.content).css('height', config.options.height);
    	}
    	
	});

});
