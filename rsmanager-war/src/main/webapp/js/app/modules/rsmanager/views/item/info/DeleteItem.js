define([
	'backbone',
	'backbone.marionette',
	'app/application',
	'hbs!tmpl/rsmanager/item/deleteitem'
],
function(Backbone, Marionette, App, PropDelete) {
	'use strict';
	/* Return a ItemView class definition of Delete Item Message */
	return Marionette.ItemView.extend({
		template: PropDelete,
		className: 'b-popup',
		events: {
			'click ._ok': 'ok',
			'click ._cancel': 'cancel',
		},
		
		initialize: function(options) {
		   if (options.sender) {
			   this.sender = options.sender;
		   }
		   this.selected = options.selected;
		   if (this.selected.length == 1) {
			   this.model = new Backbone.Model({name: this.selected[0].get('name')});
		   } else {
			   this.model = new Backbone.Model({name: this.selected.length});
		   }
		},
		
	    
	    ok: function() {
	      if (this.sender)
	    	  this.sender.trigger('filedelete');
		   this.close();
	    },
	    
	    cancel: function() {
	       this.close();
	    }
	    
    	


	});
});