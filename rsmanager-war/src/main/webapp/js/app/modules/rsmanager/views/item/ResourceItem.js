define([
	'jquery',
	'backbone',
	'underscore',
	'backbone.marionette',
	'app/application',
	'hbs!tmpl/rsmanager/item/resourcetreeitem',
	'app/modules/rsmanager/collections/Resource',
	'app/modules/rsmanager/views/item/AbstractTreeItem'
],
function($, Backbone,_, Marionette, App, Template,  Resource, TreeItem) {
	'use strict';
	
	/* Return a ItemView class definition of Resource Tree */
	return TreeItem.extend({
		template: Template,
		tagName: 'li',
		className: 'folder-tree_list-item',
		
		ui: {
			loader: 'div div.folder-tree_list-item-folder-loader',
			itemEl : '.folder-tree_list-item-folder',
			contentDiv : '.folder-tree_list-item-folder'
			
		},
		
		events : {
			'click .folder-tree_list-item-folder-trigger-icon' : 'clickArrow',
			'click .folder-tree_list-item-folder' : 'clickSelect'
		},
		
		props: {
			openClass : '-opened',
			selectClass: '-active'
		},
		
		initialize: function() {
			this.listenTo(App.vent, 'tree:select', this._unselect);
			this.listenTo(this.model,'selectMe', this.selectMe);
			this.listenTo(this.model,'navigateMe', this.navigateMe);
			this.listenTo(this.model,'change:name', this.render);
		},
		
	
		_open: function() {
			
			this.Resourcecollection = new Resource([],{volumeId: this.model.get('volumeId'),
				parent: this.model.get('absolutePath').replace(/\\/g,'%5C')});
			//this.listenTo(this.Resourcecollection,'sync', this.load);
			//COMMAND LIST
			this.showLoader();
			this.Resourcecollection.fetch({async:false, loader: this, wait: true, error: function(models, response) {
				App.vent.trigger('rsmanager:error', response);
			}});
			this.listenTo(this.Resourcecollection,'openEvent', this.openChild);
			this.listenTo(this.Resourcecollection,'reload', this.reload);
			this.load();
		},
		
		load: function() {
			this.ResourceView = require('app/modules/rsmanager/views/composite/Resource');
			this.Resourceview = new this.ResourceView({collection:this.Resourcecollection});
			this.$subtree = this.$el.append('<ul></ul>').find('ul');
			if (!this.isOpen()) {
				this.$subtree.hide();
			}
			this.Resourceview.setElement(this.$subtree).render();
			
		}
		
	
	
	});
});
