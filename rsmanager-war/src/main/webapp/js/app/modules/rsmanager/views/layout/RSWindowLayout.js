define([
     'jquery',   
	'backbone.marionette',
	'underscore',
	'app/application',
	'app/modules/rsmanager/rsconfig',
	'hbs!tmpl/rsmanager/layout/rswindow',
	'app/modules/rsmanager/views/layout/RSHeaderLayout',
	'app/modules/rsmanager/views/layout/RSLeftLayout',
	'app/modules/rsmanager/views/layout/RSContentLayout',
	'app/modules/rsmanager/views/layout/RSFooterLayout',
	'app/modules/rsmanager/views/layout/ContextMenuLayout',
	'app/modules/rsmanager/views/region/PopupRegion',
	'jquery.resizer',
],
/**
 * @author SBT-Shiryaev-DmV
 */
function($, Marionette, _, App, config, RSWindow, HeaderPanel, LeftPanel , MainContent, FooterPanel, ContextMenu,  Popup, resizer) {
    'use strict';
	/* Return a Main RsManagerLayout class definition */
	return Marionette.Layout.extend ({
		
		template: RSWindow,
		className: 'b-finder',
    	
		regions: {
    		fheader: '.finder_header-layout',
    		fleft: '.finder_aside-layout',
    		fmain: '.finder_main-layout',
    		ffooter: '.finder_footer-layout',
    		popup: Popup, 
    		contextmenu: '.finder_context-menu'
    	},
    	
    	initialize: function(options) {
    		config.extendsOptions(options);
    		this.HeaderPanel = new HeaderPanel();
    		this.LeftPanel = new LeftPanel();
    		this.MainContent = new MainContent();
    		this.FooterPanel = new FooterPanel();
    		this.ContextMenu = new ContextMenu();
    		$(document).bind('keydown.rsmanager click.rsmanager', function(evt) {
    			if (evt.keyCode == 27) {
    	   		  App.vent.trigger('rsmngr:clickesc');
    			}
    			if (evt.type == 'click') {
    			  App.vent.trigger('rsmngr:msclick');
    			}
    		});
    		this.listenTo(App.vent,'tree:select', this.onSelectDirectory);
    		//this.listenTo(App.vent,'file:add', this.onAddFileToUpload);
    		this.listenTo(App.vent,'rsmanager:clickfile', this.openFile);
    		this.listenTo(App.vent,'rsmanager:opendialog', this.openDialog);
    	},
    	
    	onRender: function() {
    	  this.$el.css('width', config.options.width + 'px');
  	      this.fheader.show(this.HeaderPanel);
  	      this.contextmenu.show(this.ContextMenu);
  	      this.fleft.show(this.LeftPanel);
  	      this.fmain.show(this.MainContent);
  	      this.ffooter.show(this.FooterPanel);
  	    },
  	    
  	    // open popup dialog
  	    // view wich loaded to popup
  	    openDialog: function(view) {
  	    	this.popup.show(view);
  	    },

        setPopupPosition: function() {
            var popup = this.el.querySelector('.b-popup');
            if ( popup.getBoundingClientRect().top < 0 ) this.el.scrollIntoView();
        },

    	/* on first rsmanager show we try to select somethig by fefault*/ 
    	onShow: function() {
    		this.LeftPanel.select();

    		if(config.options.showResizer == true) {
    		    var $finderAside = $('.finder_aside'),
    		        $finderAsideLayout = $('.finder_aside-layout'),
    		        $finderMain = $('.finder_main');
    		    
    		    $finderAsideLayout.resizer( {type: 'aside', container: this.$el, triggerWrapper: $finderAside} );
    		    this.$el.resizer( {type: 'both-sides', heightRegEls: [$finderAside, $finderMain]} );
    		}
    	},
    	
    	onClose: function() {
    		$(document).unbind('keydown.rsmngr click.rsmngr');
    	},
    	
    	/* When user click new dir as current */
    	onSelectDirectory: function(resources, currdir) {
    		this.currdir = currdir;
    		this.resources = resources;
    		this.MainContent.setFiles(resources);
    	}
	});
});
