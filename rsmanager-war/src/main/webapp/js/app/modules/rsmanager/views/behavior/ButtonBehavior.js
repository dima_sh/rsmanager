define([ 
    'backbone',
    'underscore',
	'backbone.marionette',
	'app/application',
	'app/modules/rsmanager/rsconfig',
	'app/modules/rsmanager/views/item/info/DeleteItem',
	'app/modules/rsmanager/views/item/info/ResourceInfo'
],
function(Backbone,_ , Marionette, App, config, DeleteView, Info) {
	'use strict';
	/* Return a Behaviour for rsmanager operations */
	return Marionette.Behavior.extend({
	     
		ui: {
			uploadButton: '._upload',
			createDirButton: '._new-folder',
			forwardButton: '._forward',
			backButton: '._back',
			downloadButton: '._download',
			cutButton: '._cut',
			copyButton: '._copy',
			pasteButton: '._paste',
			renameButton: '._rename',
			deleteButton: '._delete',
			reloadButton: '._loop',
			infoButton: '._info'
		},
		
		events: {
			'click @ui.createDirButton' : 'clickNewFolder',
			'click @ui.downloadButton' : 'clickDownload',
			'click @ui.cutButton' : 'clickCut',
			'click @ui.copyButton' : 'clickCopy',
			'click @ui.pasteButton' : 'clickPaste',
			'click @ui.renameButton' : 'clickRename',
			'click @ui.deleteButton' : 'clickDelete',
			'click @ui.reloadButton' : 'clickReload',
			'click @ui.infoButton' : 'clickInfo',
		},
		
		onShow: function() {
			var me = this;
			_(config.options.off).each(function(item){
				if (me.ui[item + 'Button']) {
					me.ui[item + 'Button'].hide();
				}
					
			}); 
		},
		
		initialize: function() {
			this.markedModels = [];
		    this.listenTo(App.vent,'tree:select', this.onSelectDirectory);
		    this.listenTo(this,'filedelete', this.deleteFile);
		    
		},
		// Select tree event
        onSelectDirectory: function(resources, currdir) {
        	if (this.resources) {
        		this.resources.off('select unselect');
    		}
            this.currdir = currdir;
            this.resources = resources;
            
        	this.resources.on('select unselect', this.selectEvent, this);
    		this.view.setEnabled([this.ui.uploadButton, this.ui.createDirButton], !this.isSearch());
    		this.selectEvent();

        },
        // initialize operation after select event
        selectEvent: function() {
    		var selected = this.resources.bySelected().length;
   			this.view.setEnabled([this.ui.deleteButton, this.ui.cutButton,this.ui.copyButton], (selected > 0) && !this.isSearch());
   			this.view.setEnabled([this.ui.renameButton], selected == 1 && !this.isSearch());
   			this.view.setEnabled([this.ui.downloadButton], this.resources.byFileSelected().length > 0);
   			this.view.setEnabled([this.ui.infoButton], this.resources.bySelected().length > 0);
   			this.view.setEnabled([this.ui.reloadButton], this.resources);
   			this.view.setEnabled([this.ui.pasteButton], (this.markedModels && this.markedModels.length > 0) && !this.isSearch());
   			
    	},
    	//check that current dir is search tag
    	isSearch: function() {
    		return this.currdir.isSearchTag();
    	},

        //click upload button
        clickUpload : function() {
        	if (this.currdir)
              this.ui.fileinput.click();
        },
      
        //click delete button   
        clickDelete: function(evt) {
            evt.preventDefault();
            if (this.resources.bySelected()[0] && !this.isSearch())
            	this.onConfirmDelete();
        },
        //click new folder button
        clickNewFolder: function(evt) {
            evt.preventDefault;
            if (this.currdir && !this.isSearch())
              App.vent.trigger('dir:create');
            return false;
        },
        //click cut button
        clickCut: function(evt) {
        	if (this.currdir && !this.isSearch()) {
        		if (this.markedModels) {
        			_(this.markedModels).each(function(model){
        				model.trigger('uncut');
        			});
        		}
        		this.markedModels = this.resources.bySelected();
        		_(this.markedModels).each(function(model){
        			//PREPARE COMMAND UPDATE(CUT)
        			model.setOperation('cut');
        			model.trigger('cut');
        		});
        		this.view.setEnabled([this.ui.pasteButton], (this.markedModels && this.markedModels.length > 0));
        	}
        },
        //click copy button
        clickCopy: function(evt) {
        	if (this.currdir && !this.isSearch()) {
        		if (this.markedModels) {
        			_(this.markedModels).each(function(model){
        				model.trigger('uncut');
        			});
        		}
        		this.markedModels = this.resources.bySelected();
        		_(this.markedModels).each(function(model){
        			//PREPARE COMMAND UPDATE(COPY)
        			model.setOperation('copy');
        		});
        		this.view.setEnabled([this.ui.pasteButton], (this.markedModels && this.markedModels.length > 0));
        	}
        },
        //click paste button
        clickPaste: function(evt) {
        	evt.preventDefault();
        	var me = this;
        	if (this.currdir && this.resources && this.markedModels && !this.isSearch()) {
        		var sourceCollection = this.markedModels[0];
    			_(this.markedModels).each(function(model){
    				model.set({'newParent': me.currdir.get('absolutePath')});
    				//COMMAND UPDATE(REPLACE, COPY)
    				model.save([],{
    					wait:true,
    					async: false,
    					showglobalerr: false,
    					loader: {
    						showLoader: function() {
    							me.view.setBuzy(me.ui.pasteButton);
    						},
    						hideLoader: function() {
    							me.view.setReady(me.ui.pasteButton);
    						}
    					},
    					error: function(model, response) {
    						App.vent.trigger('rsmanager:error', response.responseJSON);
    					}
    				});
    			});
    			
    			if (this.resources)
    			  this.resources.trigger('reload');
    			if (sourceCollection)
    			  sourceCollection.trigger('reload');
    			this.markedModels = null;
    			this.view.setEnabled([this.ui.pasteButton], (this.markedModels && this.markedModels.length > 0));
        	}
        },
        //click reload button
        clickReload: function(evt) {
        	evt.preventDefault();
        	if (this.resources)
        	  this.resources.trigger('reload');
        },
        //click rename button
        clickRename: function(evt, resources) {
            evt.preventDefault();
            if (this.resources.bySelected()[0] && !this.isSearch())
              this.resources.bySelected()[0].trigger('rename');
        },
        //click download button
        clickDownload: function(evt) {
    	   evt.preventDefault();
    	   this.downloadResource();
	    },
	    //download clicked resource
	    downloadResource: function() {
	    	if (this.resources.bySelected()[0]) {
		    	_(this.resources.byFileSelected()).each(function(model) {
		    		//COMMAND DOWNLOAD
	   				window.open(App.baseUrl + '/resource/get/' 
	   						+ model.get('absolutePath').replace(/\\/g,'%5C'),'','width=700,heigth=500,left=50,top=50');
	    		});
	    	}
	    },
	    
		//Confirm deleting file action
    	onConfirmDelete: function() {
    		var selectedmodels = this.resources.bySelected();
    		if (selectedmodels.length != 0 ) { 
    		  App.vent.trigger('rsmanager:opendialog', new DeleteView({selected: selectedmodels ,sender: this}));
    		}
    	},
    	//clickInfo command
    	clickInfo: function(evt) {
    		evt.preventDefault();
    		if ((this.resources.bySelected())) {
    			var info = this.resources.getSelectedInfo();
    			var model = info.model;
    			if (info.total > 1) {
    				model = new Backbone.Model(info);
    			}
    			App.vent.trigger('rsmanager:opendialog', new Info({model: model}));	
    		}
    		
    	},
    	
    	// Delete file action 
    	deleteFile: function() {
    		var me = this;
    		var errors_ = [];
    		_(this.resources.bySelected()).each(function(model){
      			   //COMMAND DELETE
    				model.destroy({
        				showglobalerr: false,
        				async: false,
        				wait: true,
        				loader: {
    						showLoader: function() {
    							me.view.setBuzy(me.ui.deleteButton);
    						},
    						hideLoader: function() {
    							me.view.setReady(me.ui.deleteButton);
    						}
    					},
        				error: function(model, response) {
        					if (response.status != 200) {
        						errors_.push(response.responseJSON);
        					}
        					  
        				}
    				});
    		});
    		
    		if (errors_.length) {
				App.vent.trigger('rsmanager:error', errors_);
    		}
    	 }
		
	});
});
