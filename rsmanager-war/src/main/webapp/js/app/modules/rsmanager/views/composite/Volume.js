define([ 
    'backbone',
    'underscore',
	'backbone.marionette',
	'app/application',
	'app/modules/rsmanager/views/item/VolumeItem',
	'hbs!tmpl/rsmanager/composite/volumetreeview'
	
],
function(Backbone,_ , Marionette, App, VolumeItem, Template) {
	'use strict';
	/* Return a CompositeView class definition of Volume model */
	return Marionette.CompositeView.extend({
		template: Template,
		
		className: 'finder_col _p-reset _w-full',
		
		itemViewContainer: '.folder-tree_list',
		
		itemView: VolumeItem
		
	
	});
});