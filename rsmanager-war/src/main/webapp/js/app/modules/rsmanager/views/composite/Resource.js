define([ 
    'backbone',
    'underscore',
	'backbone.marionette',
	'app/application',
	'app/modules/rsmanager/views/item/ResourceItem',
	'hbs!tmpl/rsmanager/composite/resourcetreeview'
	
],
function(Backbone,_ , Marionette, App, ResourceItem, Template) {
	'use strict';
	/* Return a CompositeView class Resource View definition to show on Tree */
	return Marionette.CompositeView.extend({
		template: Template,
		
		className: 'folder-tree_list',
		
		itemView: ResourceItem,
		
		initialize: function() {
			this.collection.on('sync', this.render, this);
		},
		//filter to show on tree just directory resources
		addItemView: function (item, ItemView, index) {
		 if (item.get('mimeTypes') == 'directory') {
			Backbone.Marionette.CompositeView.prototype.addItemView.apply(this,arguments);	
		 }
		} 
	
	});
});