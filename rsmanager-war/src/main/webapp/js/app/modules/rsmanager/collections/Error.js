define([
    'underscore',
    'backbone',
	'app/modules/rsmanager/models/Error',
	'app/application'
],
function(_, Backbone, Error, App) {
	'use strict';
	/* Return a collection class definition of Error model */
	return Backbone.Collection.extend({	
		model: Error
		
	});
});