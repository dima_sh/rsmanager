define([ 
    'backbone',
    'underscore',
	'backbone.marionette',
	'app/application',
	'app/modules/rsmanager/views/item/info/ErrorItem',
	'hbs!tmpl/rsmanager/composite/rserrorview'
	
],
function(Backbone,_ , Marionette, App, ErrorItem, Template) {
	'use strict';
	/* Return a CompositeView class definition of error view */
	return Marionette.CompositeView.extend({
		template: Template,
		tagName: 'ul',
		className: 'error-box_list',
		itemView: ErrorItem,
	
	});
});