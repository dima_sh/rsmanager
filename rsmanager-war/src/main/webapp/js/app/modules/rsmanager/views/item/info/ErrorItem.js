define([
	'jquery',
	'backbone',
	'underscore',
	'backbone.marionette',
	'app/application',
	'hbs!tmpl/rsmanager/item/rserroritem',
	'app/modules/rsmanager/collections/Resource'
],
function($, Backbone,_, Marionette, App, Template,  Resource) {
	'use strict';
	/* Return a Error ItemView class definition */
	return Marionette.ItemView.extend({
		template: Template,
		tagName: 'li',
		className: 'error-box_list-item',
	});
});
