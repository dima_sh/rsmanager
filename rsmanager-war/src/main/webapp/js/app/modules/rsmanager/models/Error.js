define([
	'underscore',
	'backbone',
	'app/application',
	'app/modules/rsmanager/rsconfig'
],
function(_, Backbone, App, config) {
	'use strict';
	/* Return a model class definition of Error message */
	return Backbone.Model.extend({
		defaults: {
			userMessage: function() {
				return this.code;
			}
		},
	    // try to parse error model
		parse: function(resp, options) {
			var error = {};
			if (resp == undefined) {
	    		error.code = 'UNKNOW';
	    		error.details = 'undefined exception';
			} else if (resp.code != undefined) {
				error.code = resp.code;
				error.details = resp.details;
				error.type = resp.type;
			} else if (resp.responseJSON) {
	    		error = resp.responseJSON;
	    	} else if (resp.statusText) {
	    		error.code = resp.statusText;
	    		error.details = resp.responseText;
	    		console.warn(error.message);
	    	} else {
	    		error.code = 'UNKNOWN';
	    		error.details = 'unparsed exception see error log';
	    		console.error(resp);	
	    	}
			
			var type = _.find(config.options.messages.errors, function(item, key){return error.code == key;});
			if (type)
				error.code = type;
			console.warn(error.message);
    	    return error;
	    }
    });
});