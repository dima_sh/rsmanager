define([
    'jquery',
	'backbone.marionette',
	'underscore',
	'app/application',
	'app/modules/rsmanager/rsconfig',
	'hbs!tmpl/rsmanager/layout/rsheader',
	'app/modules/rsmanager/models/Volume',
	'app/modules/rsmanager/models/Error',
	'app/modules/rsmanager/views/behavior/ButtonBehavior',
	'app/modules/rsmanager/views/behavior/UploadBehavior',
],
/**
 * @author SBT-Shiryaev-DmV
 */
function($, Marionette, _, App, config, Template, Resources, Error, ButtonBehavior, UploadBehavior ) {
    'use strict';
	/* Return a Header Panel Layout class definition */
	return Marionette.Layout.extend ({
		
		className:'finder_header',
		template: Template,

		ui: {
			forwardButton: '._forward',
			backButton: '._back',
			downloadButton: '._download',
			uploadProgress: '#uploadAction',
			finderfilter: 'input#FinderFilter'	
		},
		
		behaviors: {
			ButtonBehavior: {
				behaviorClass : ButtonBehavior,
			},
		    UploadBehaviour: {
		    	behaviorClass : UploadBehavior,
		    }
		},
		
		events: {
			'click @ui.forwardButton' : 'clickForward',
			'click @ui.backButton' : 'clickBack',
			'click .input-filter_btn' : 'clickFilter' 
		},
		
		initialize: function(options) {
			this.historyPointer = 0;
			this.listenTo(App.vent,'tree:select', this.onSelectDirectory);
		    this.listenTo(App.vent,'rsmanager:clickfile', this.download);
		},
		
		historyPath: [],
		
		// handle event on select directory
		onSelectDirectory: function(resources, currdir) {
    		this.currdir = currdir;
    		this.resources = resources;
    		var path = this.currdir.get('absolutePath') == ''? this.currdir.get('name'): this.currdir.get('absolutePath');
    		if (this.historyPath[this.historyPointer] != path) {
        		this.historyPath.push(path);
        		localStorage.setItem('rsmanager:historypath', path);
        		this.historyPointer = this.historyPath.length - 1; 
    		}
    		this.setEnabled([this.ui.forwardButton],this.historyPointer < this.historyPath.length-1);
    		this.setEnabled([this.ui.backButton],this.historyPointer > 0); 
    	},

	  
	    // mark the actionElement (for example button)
	    // as now is buising
	    // @el - is element links
	    setBuzy: function(el) {
	    	el.addClass('_loader');
	    },
	    // the action hav ended, the ux element ready
	    // @el - is element links
	    setReady: function(el) {
	    	el.removeClass('_loader');
	    },
	    
	    //set the btn enabled on header panel
	    //@btns - btn array
	    //@enabled - if true btn enabled othewise desabled
	    setEnabled: function(btns, enabled) {
	    	_(btns).each(function(btn) {
	    		if (enabled) {
	    			btn.removeClass('-disabled');	
	    		} else {
	  	    	  if (!btn.hasClass('-disabled')) 
	 	    		  btn.addClass('-disabled');
	    		}
	    	});
	    },
	    // click Forward button
	    clickForward: function(evt) {
	    	evt.preventDefault();
	    	if (this.historyPointer < this.historyPath.length -1) {
	    			App.vent.trigger('rsmanager:navtree', this.historyPath[++this.historyPointer]);		
	    	} 
	    },
	    //click Back button
	    clickBack: function(evt) {
	    	evt.preventDefault();
	    	if (this.historyPointer > 0) {
	    		App.vent.trigger('rsmanager:navtree', this.historyPath[--this.historyPointer]);
	    	}
	    },
	    //click filter button
	    clickFilter: function(evt) {
	    	evt.preventDefault();
	    	if (this.ui.finderfilter.val()) {
	    		var mask = this.ui.finderfilter.val();
	    		App.vent.trigger('rsmanager:search', mask);
	    	}
	    },
	    // download command
	    download: function(model) {
	    	if (!config.options.setUrl)
	    	  window.open(App.baseUrl + '/resource/get/' 
						+ model.get('absolutePath').replace(/\\/g,'%5C'),'','width=700,heigth=500,left=50,top=50');
	    }
	    
	});

});
