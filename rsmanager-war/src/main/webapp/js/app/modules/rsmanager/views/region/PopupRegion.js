define([
    'jquery',
    'underscore',    
	'backbone',
	'backbone.marionette',
	'app/application',
	'bootstrap'

],
function($, _ ,Backbone, Marionette, App) {
	'use strict';  
   return Backbone.Marionette.Region.extend({
	  // return popup region for local modal message of rsmanager
      el: '.finder_popup-layout',
      
      constructor: function(){
        _.bindAll(this, 'showModal', 'hideModal');
        Backbone.Marionette.Region.prototype.constructor.apply(this, arguments);
        this.on("show", this.showModal);
      },
  
 
      getEl: function(selector){
        var $el = Backbone.$(selector);
        $el.on("hidden", this.close);
        return $el;
      },
 
      showModal: function(view){
        view.on("close", this.hideModal, this);
        $(this.el).show();
      },
 
      hideModal: function(){
       this.$el.hide();
       this.reset();
     }
  });
   

});
