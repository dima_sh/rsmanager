define([
	'underscore',
	'backbone',
	'app/application',
],
function(_, Backbone, App) {
	'use strict';
	/* Return a model class definition of Volume */
	return Backbone.Model.extend({	
		url: 'resource/volume',
		
		initialize: function() {
			this.set('absolutePath', this.get('name'));
		},
		// check that is volume is search Volume
		isSearchTag: function() {
			return this.get('type') +'' == 'search';
		}
		
	
    });
});