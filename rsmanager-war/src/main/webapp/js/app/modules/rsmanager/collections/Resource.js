define([
    'underscore',
    'backbone',
	'app/modules/rsmanager/models/Resource',
	'app/application',
	'app/modules/rsmanager/rsconfig'
],
function(_, Backbone,  Resource, App, config) {
	'use strict';
	/* Return a collection class definition of Resource model*/
	return Backbone.Collection.extend({	
		model: Resource,
		url: function() {
			return '/resource/'+ this.parent;	
		},
		
		sortAttribute: 'default',
		sortDirection: 1,
		
		initialize: function(models, options) {
			//this.searhresult = options.searchresult;
			this.volumeId = options.volumeId;
			this.parent = options.parent != undefined ? options.parent : '' ;
			this.on("add", function(model){
				model.set('volumeId',this.volumeId);
				model.set('parent', this.parent);
			});
			
		},
		
		onRender: function() {
			this.collection.sortResources();
		},
		
		sortResources: function(attr) {
			if (attr)
			  this.sortAttribute = attr;
			this.sort();
		},
		
		comparator: function(m1, m2) {
			var isDefault = this.sortAttribute == 'default';
			var sortAttribute = isDefault ? 'modifiedDate': this.sortAttribute;
			var a = m1.get(sortAttribute);
			var b = m2.get(sortAttribute);
			if (isDefault) {
				var mime1 = m1.get('mimeTypes');
				var mime2 = m2.get('mimeTypes');
				if (mime1 == 'directory' && mime2 != 'directory') {
				   return  0 - this.sortDirection;	
				}  else if (mime1 != 'directory' && mime2 == 'directory')  {
					return 0 + this.sortDirection;
				}
			}
			if (a == b) return 0;
			
			if (this.sortDirection == 1) {
				return a > b ? 1 : -1;
			} else {
				return a < b ? 1 : -1;
			}
		},
		
		sync: function(method, collection, options) {
			Backbone.Collection.prototype.sync.apply(this,arguments);
			collection.invoke('set',{volumeId: this.volumeId});
		},
		// find selected resources in collection
		bySelected: function() {
			var filtered = this.filter(function(model) {
				return model.isSelected();
			});
			return filtered;
		},
		// find just file selected (not directory)
		byFileSelected: function() {
			var filtered = this.filter(function(model) {
				return (model.isSelected() && model.get('mimeTypes') != 'directory') ;
			});
			return filtered;
		},
		
		parse: function(response) {
			return response.resources;
		},
		// check that parent is search volume
		isSearchTag: function() {
			return config.isSearchTag(this.volumeId);
		},
		// generate short information about selected resources
		getSelectedInfo: function() {
			var info = {
				total:0,
				name: '',
				folder: 0,
				file: 0,
				model: undefined,
				size:0
			};
			_(this.bySelected()).each(function(model){
				info.name = model.get('name');
				info.model = model;
				if (model.get('mimeTypes') == 'directory') {
					info.folder +=1;
				} else {
					info.file +=1;
				}
				info.total +=1;
				info.size += model.get('size');;
			});
			return info;
		}
		
	});
	
});