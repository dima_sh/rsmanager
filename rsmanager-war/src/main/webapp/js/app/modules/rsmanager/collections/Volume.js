define([
    'underscore',
    'backbone',
	'backbone.paginator',
	'app/modules/rsmanager/models/Volume',
	'app/application'
],
function(_, Backbone, PageableCollection, Volume, App) {
	'use strict';
	/* Return a collection class definition of Volume */
	return Backbone.Collection.extend({	
		model: Volume,
		url: '/resource/volume'
	});
});