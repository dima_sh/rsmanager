define([
	'backbone.marionette',
	'underscore',
	'app/application',
	'hbs!tmpl/rsmanager/layout/rsfooter',
],
function(Marionette, _, App, Template ) {
    'use strict';
	/* Return a Footer Layout class definition */
	return Marionette.Layout.extend ({
		className:'finder_footer',
		template: Template,
		
		ui: {
			dirpath: '.finder_footer-left',
			dirinfo: '.finder_footer-right',
			iteminfo: '.finder_footer-center'	
		},
		
		initialize: function() {
			this.listenTo(App.vent, 'tree:select', this.onSelectDirectory);
		},
		// select directory evvent
		//@resources - opened resources
		//@currdir - current selected parent dir
    	onSelectDirectory: function(resources, currdir) {
    		if (this.resources) {
        		this.resources.off('select unselect');
    		}
    		
    		this.currdir = currdir;
    		this.resources = resources;
    		this.listenTo(this.resources, 'select unselect', this.onSelect);
    		var size = 0;
    		var items = 0;
    		_(this.resources.models).each(function(model) {
    			size += model.get('size');
    			items += 1;
    		});
    		this.ui.iteminfo.html('');
    		this.ui.dirpath.html(this.currdir.get('absolutePath') == ''? this.currdir.get('name'): this.currdir.get('absolutePath'));
    		this.ui.dirinfo.html('items:' + items + ', size: ' + App.calculateSize(size));
    	},
    	
    	onSelect: function() {
    		this.showSelected();
    	},
    	//show selected file information
    	showSelected: function() {
    		if (!this.resources.models) {
    			return;
    		} 
    		var info = this.resources.getSelectedInfo();
    		if (info.total == 1) {
    			this.ui.iteminfo.html(info.name + ', size: ' + App.calculateSize(info.size));
    		} else if (info.total > 1) {
    			this.ui.iteminfo.html('selected items: ' + info.total + ', size: ' + App.calculateSize(info.size));
    		} else {
    			this.ui.iteminfo.html('');
    		}
    	}
    	
	});

});
