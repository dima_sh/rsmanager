define(['underscore','handlebars'],
/**
 * @author SBT-Shiryaev-DmV
 */
function (_, HandlerBar) {
	'use strict';
	//Return rs manager config 
	var config = function() {
		return {
			//default options
			options: {
				// size of UI window
				width:1100, 
				height:700,
				// operation that not show on header panel
	    		off: [],
	    		apiUrl: '',
	    		// volume prefix constatnts
	    		volumeTags: {
	    		  search: 's_',
	    		  favorite: 'f_',
	    		  bucket: 'b_'
	    		},
	    		//localization
	    		messages: {
	    		    headerResourceManager: 'Менеджер ресурсов',
	    		    refreshButton: 'Обновить',
	    			cutButton: 'Вырезать',
	    			copyButton: 'Скопировать',
	    			pasteButton: 'Вставить',
	    			renameButton: 'Переименовать',
	    			deleteButton: 'Удалить',
	    			downloadButton: 'Скачать',
	    			uploadButton: 'Загрузить',

	    			backButton: 'Назад',
	    			forwardButton: 'Вперед',
	    			mkdirButton: 'Создать папку',
	    			filterPlaceholder: 'Фильтр...',
	    			//errors loaclizations
		    		errors: {
		    			UNKNOWN: 'UNKNOWN ERROR', 
		    			DELETE: 'Ошибка при удалениии ресурса', 
		    			CREATE: 'Ошибка создания ресурса', 
		    			RENAME: 'Ошибка при переименовании ресурса', 
		    			CUTPASTE: 'Ошибка ппереноса ресурса(ов)', 
		    			COPYPASTE: 'COPYPASTE', 
		    			UPDATEBADREQUEST : 'UPDATEBADREQUEST' , 
		    			UNSUPPORTEDOPERATION: 'UNSUPPORTEDOPERATION',
		    			SEARCH: 'SEARCH',
		    			LIST: 'LIST',
		    			GETRESOURCE: 'GETRESOURCE',
		    			DELETEDENY: 'DELETEDENY',
		    			CREATDENY: 'CREATDENY',
		    			RENAMEEXIST: 'RENAMEEXIST',
		    			RENAMEFAIL: 'RENAMEFAIL',
		    			RENAMEDENY: 'RENAMEDENY',
		    			VOLUMEGET: 'VOLUMEGET',
		    			VOLUMEFIND: 'VOLUMEFIND',
		    			NODIRCONTENT: 'NODIRCONTENT',
		    			GETCONTENTDENY: 'GETCONTENTDENY',
		    			GETCONTENT:'GETCONTENT'
		    		}
	    		},
	    		showResizer: true
	    	},
	    	//extendig default options
			extendsOptions: function(options) {
				this.options = _.defaults(options || {}, this.options);
			},
			findMessage: function(message) {
				return _.find(this.options.messages, function(item, key){return message == key;});
			},
			
			isSearchTag: function(volumeId) {
				return (volumeId + '').indexOf(this.options.volumeTags.search) == 0;
			}
		};
	};
	
	var config = new config; 
	
	 HandlerBar.registerHelper('rsMessage',function(message, defaultMessage){
			return config.findMessage(message) || defaultMessage;
	 });
	
    return config;
});
