define([
	'backbone.marionette',
	'app/application',
	'hbs!tmpl/rsmanager/layout/contextmenu',
	'app/modules/rsmanager/views/behavior/ButtonBehavior',
	'app/modules/rsmanager/views/behavior/UploadBehavior',
],
function(Marionette, App, Template, ButtonBehavior, UploadBehavior) {
    'use strict';
	/* Return a Context Menu Layout class definition */
	return Marionette.Layout.extend ({
		template: Template,
		className: 'b-context-menu',
		
		ui: {
			uploadProgress: ''
		},
		
		behaviors: {
			ButtonBehavior: {
				behaviorClass : ButtonBehavior,
			},
		    UploadBehaviour: {
		    	behaviorClass : UploadBehavior,
		    }
		},
		
		initialize: function() {
			this.$el.hide();
			this.$el.css('position', 'absolute');
			this.listenTo(App.vent,'rsmanager:contextmenu', this.showContextMenu);
			this.listenTo(App.vent,'rsmngr:msclick', this.hideContextMenu);
		},
	    // calculate coordinate and show context menu on right click
		//@e - event
	    showContextMenu: function(e) {
	    	var pos = this.getPosition(e);
            this.$el.parents().filter(function() { 
                return $(this).css('position') == 'relative'; 
             }).each(function() {
                 pos.x = pos.x - $(this).position().left;
                 pos.y = pos.y - $(this).position().top;
             })
	    	this.$el.css('left',pos.x + "px");
	    	this.$el.css('top',pos.y + "px");
	    	
	    	this.$el.show();
	    },
	    
	    
	    setEnabled: function(btns, enabled) {
	    	this.hideContextMenu();
	    	_(btns).each(function(btn) {
	    		if (enabled) {
	    			btn.removeClass('-disabled');	
	    		} else {
	  	    	  if (!btn.hasClass('-disabled'))
	 	    		  btn.addClass('-disabled');
	    		}
	    	});
	    },
		
		hideContextMenu: function() {
			this.$el.hide();
		},
		
		  
	    // mark the actionElement (for example button)
	    // as now is buising
	    // @el - is element links
	    setBuzy: function(el) {
	    	el.addClass('_loader');
	    },
	    // the action hav ended, the ux element ready
	    // @el - is element links
	    setReady: function(el) {
	    	el.removeClass('_loader');
	    },
	    // get mouse current position
	    getPosition: function(e) {
	    	  var posx = 0;
	    	  var posy = 0;
	    	 
	    	  if (!e) var e = window.event;
	    	 
	    	  if (e.pageX || e.pageY) {
	    	    posx = e.pageX;
	    	    posy = e.pageY;
	    	  } else if (e.clientX || e.clientY) {
	    	    posx = e.clientX + document.body.scrollLeft + 
	    	                       document.documentElement.scrollLeft;
	    	    posy = e.clientY + document.body.scrollTop + 
	    	                       document.documentElement.scrollTop;
	    	  }
	    	 
	    	  return {
	    	    x: posx,
	    	    y: posy
	    	  }
	    	}

	
		
	});

});