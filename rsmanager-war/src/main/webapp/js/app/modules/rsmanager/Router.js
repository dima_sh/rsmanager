define([
	'backbone.marionette',
	'app/application',
	'app/modules/rsmanager/Controller',
],
function(Marionette, App, RsManagerController){
	'use strict';
	// return router object
	var RsManagerRouter = Marionette.AppRouter.extend({
		appRoutes: {
            '' : 'showRsManager'
		}
	});
  
	App.addInitializer(function(options) {
		new RsManagerRouter({controller: new RsManagerController()});
	});
	
	return RsManagerRouter;
});