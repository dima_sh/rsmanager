define([
	'jquery',
	'backbone',
	'underscore',
	'backbone.marionette',
	'app/application',
	'app/modules/rsmanager/collections/Resource',
	'app/modules/rsmanager/views/composite/Resource'
	
],
function($, Backbone, _, Marionette, App, Resource, ResourceView) {
	'use strict';
	/* Return a ItemView class definition to show on Tree*/
	return Marionette.ItemView.extend({
		
		//click on expand/collapse button		
		clickArrow: function(e) {
			e.stopPropagation();
			e.preventDefault();
			this.openTree();
			return false;
		},
		//click on directory 
		clickSelect: function(e) {
			e.stopPropagation();
			e.preventDefault();
			this.selectMe();
			return false;
		},
	
		// open subresources event
		openTree: function() {
			if (!this.isOpen()) {
				this.ui.itemEl.addClass(this.props.openClass);
				if (this.Resourcecollection == null) {
					this._open();	
					//this.load();
				} else {
					if (this.$subtree) {
						this.$subtree.show();
					}
				}
			} else {
				this.$subtree.hide();
				this.ui.itemEl.removeClass(this.props.openClass);
			}
		},
	
		// select event
		selectMe: function() {
			if (!this.isSelected()) {
				this.ui.contentDiv.addClass(this.props.selectClass);
			    this._select();
			    App.vent.trigger('tree:select', this.Resourcecollection, this.model);
			}
		},
		// check that directory is open
		isOpen: function() {
			return this.ui.itemEl.hasClass(this.props.openClass);
		},
		// check that directory is selected
		isSelected: function() {
			return this.ui.contentDiv.hasClass(this.props.selectClass);
		},
	    // command to open child in current directory
		openChild: function(model) {
			if (!this.isOpen()) 
			  this.openTree();
			var child = this.Resourcecollection.get(model.get('id'));
			if (child) {
				child.trigger('selectMe');
			}
		},
		// close view
		_close: function() {
			if (this.Resourceview) {
				this.Resourceview.close();
			}
		},
		
		// reload model		
		reload: function() {
			this._close();
			this._open();
			//this.load();
			if (this.isSelected()) {
			    App.vent.trigger('tree:select', this.Resourcecollection, this.model);
			}
		},
		// try to select model
		_select: function() {
			if (this.Resourcecollection == null) {
				this._open();
			}
		},
		// try to unselect model
		_unselect: function(collection, model) {
			if (this.model.get('id') != model.get('id')) {
				this.ui.itemEl.removeClass(this.props.selectClass);
			}
		},
		// open me if I in path
		navigateMe: function(paths) {
			if (!this.isOpen()) 
				  this.openTree();
			if (paths.length > 0) {
				var name = paths.shift();
				var item = this.Resourcecollection.findWhere({name: name});
				if (item) {
					item.trigger('navigateMe', paths);
				}
			} else {
				this.selectMe();
			}
		},
					
		
		showLoader: function() {
			this.ui.loader.show();
		},
		
		hideLoader: function() {
			this.ui.loader.hide();
		}
		
		
	
	});
});