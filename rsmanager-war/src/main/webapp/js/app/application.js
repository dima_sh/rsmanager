
define([
    'jquery',
	'backbone',
	'backbone.marionette',
	'app/views/region/ModalRegion',
    'app/views/region/Loader',
	'app/views/item/LoaderView',
   // 'cultures/ru-RU'
],
function ($, Backbone, Marionette, ModalRegion, Loader, LoaderView) {
	'use strict';
	var App = new Marionette.Application();
	App.baseUrl = './rsapi';
    
    $.ajaxSetup({
	  cache:false
	 });

    // Adds baseUrl to all urls inside this Application
    Backbone.ajax = function ajax(args) {
        return Backbone.$.ajax(App.baseUrl + args.url, args);
    };
    
    App.addRegions({
        modal: ModalRegion,
        loader: Loader,
        regionContent: '#regionContent',
        modalsHolder: '#modal-container'
	});
   
    Backbone.$(document).ajaxSend(function(event, request, settings) {
    	if (settings.loader) {
    		settings.loader.showLoader();
    	} else {
    		App.loader.show(new LoaderView());	
    		
    	}
	});

    Backbone.$(document).ajaxComplete(function(event, xhr, settings) {
    	if (settings.loader) {
    		settings.loader.hideLoader();
    	} else {
		   App.loader.hideModal();
    	}
    
    	//alert(xhr.status != 200 && settings.globalhandler === true);
    	if (xhr.status == 403) {
			  window.location.href='./';
		} else if (xhr.status != 200 ) {
			if (settings.showglobalerr != false)
			  alert('Операция временно недоступна');
		}
	});

    return App;
});