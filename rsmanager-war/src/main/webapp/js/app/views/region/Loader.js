define([
    'jquery',
    'underscore',    
	'backbone',
	'backbone.marionette',
	'app/application',
	'bootstrap'

],
function($,_,Backbone, Marionette, App) {
   'use strict';
   //return loader view
   return Backbone.Marionette.Region.extend({
      el: "#loader-container",
 
      constructor: function(){
        _.bindAll(this, 'showModal', 'hideModal');
        Backbone.Marionette.Region.prototype.constructor.apply(this, arguments);
        this.on("show", this.showModal);
      },
  
 
      getEl: function(selector){
        var $el = $(selector);
        $el.on("hidden", this.close);
        return $el;
      },
 
      showModal: function(view){
        view.on("close", this.hideModal, this);
        $(this.el).modal();
      },
 
      hideModal: function(){
       this.$el.modal('hide');
     }
  });
   

});
