define([
	'backbone',
	'backbone.marionette',
	'app/application',
	'hbs!tmpl/common/loader'
],
function(Backbone, Marionette, App, Loader) {
	'use strict';
	/* Return a ItemView class definition */
	return Marionette.ItemView.extend({
		template: Loader,
		className: 'modal'
	});
});