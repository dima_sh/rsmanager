// Backbone.hints  - behaviors of form hints. The content of hints should be defined in Backbone.Model
(function (factory) {
    if (typeof exports === 'object') {
        module.exports = factory(require('backbone'), require('underscore'), require('underscore.string'));
    } else if (typeof define === 'function' && define.amd) {
        define(['backbone', 'underscore', 'underscore.string'], factory);
    }
}(function (Backbone, _) {
        'use strict';
// ///////////////////////////////////////////////////////////////////////////////////////////////

//    /** ************ Mixin подсказок Модели ************************************** */
//    /**
//     * Примеры возможных вариантов использования можно посмотреть в комментарии свойства hint - модель

//        Usage:
//        /**
//         *  Правила отображения подсказок к полям ввода формы
//         *
//         * Available rules, when show hint
//         *
//         * Rules:
//         *   focused - just when element was focused
//         *   always - show this hint after render even with errors
//         *   always_skip_errors
//         *   once_render - show hint after render, and when element will losted focus hint dissapear
//         *   after_change - show just after property was changed (hide when blur)
//         *   after_change_always - show after property was changed (always, independent of DOM events)
//         *   after_change_anything - show hint after changed any attribute in model
//         *   not_update -  will not update after change anything in model
//         *
//         *  fnChange -  (*необязательное) выполняется когда данное свойство меняется в Модели
//         *  fn - (*необязательное) выполняется всегда
//         *
//         */
//
//
//        /**  Usage examples:
//         *
//         */
//
//        /**
//         birthPlace: {
//                text: 'Заполните как в Вашем паспорте',
//                rule: 'focused'
//            },
//
//         mobilePhone: {
//                text: 'По этому номеру оператор свяжется с Вами',
//                rule: 'once_render',
//
//                default: {
//                    text: 'По этому номеру оператор свяжется с Вами',
//                    rule: 'once_render'
//                },
//
//                //Method Appear hint - useful when before appear must execute some logic
//
//                fnChange: function () {
//                    var returnObj = _.clone(this.hints.mobilePhone.default);
//
//                    if (!_.isEmpty(this.get('mobilePhone'))) {
//                        this.hints.workPhone.rule = 'after_change_anything';
//                    }
//                    else {
//                        this.hints.workPhone.rule = this.hints.workPhone.default.rule;
//                    }
//                    if (this.get('mobilePhone') == '333' ) {
//                        this.hints.workPhone.text = 'Ваш номер мобильного в черном списке';
//                    }
//                    else {
//                        this.hints.workPhone.text = this.hints.workPhone.default.text;
//                    }
//                    return returnObj;
//                }
//            },
//
//         workPhone: {
//                text: 'Если Вы не ответите мы позвоним по рабочему номеру',
//                default :{
//                    text: 'Если Вы не ответите мы позвоним по рабочему номеру',
//                    rule: ''
//                }
//            },
//
//         homePhone: {
//                fn: function() {
//                  return {
//                      text: 'Всеравно заполните!',
//                      rule: 'always'
//                  }
//                },
//                fnChange: function() {
//                    return {
//                        text: 'Заполните домашний пожалуйста!',
//                        rule: 'always'
//                    }
//                }
//            }
//         **/


//     */
        var BackboneHints = {};

        var defaultOptions = {
            hintSelector : 'name'
        };

        BackboneHints.mixin = function(options) {
            BackboneHints.opt = _.extend(defaultOptions, options || {});
            // Hints
            return {
            bindModelHint : function(opt) {
//            /** функции отображения/скрытия - реализуются и передаются из View * */
                var view = opt.view, showHint = opt.showHint, hideHint = opt.hideHint, DOM_RULES = [ 'once_render',
                    'focused', 'after_change' ];

//            /**
//             * Слушаем пользовательские события модели связанные с отображением/скрытием подсказок, назначаем
//             * обработчики
//             */
                this.on('focus:hint', function(control) {
                    this.hintHub({
                        element : control,
//                    /** Правила при которых будет выполнятся функция отображения подсказки при фокусе* */
                        rules : DOM_RULES,
                        fn : showHint,
                        view : view
                    });
                });

                this.on('blur:hint', function(control) {
                    this.hintHub({
                        element : control,
//                    /** Правила при которых будет выполнятся функция скрытия подсказки при потере фокуса* */
                        rules : DOM_RULES,
                        fn : hideHint,
                        view : view
                    });

                });

                this.on('render:hint', function() {
                    this.observableChange(view, {}, opt, true)
                });

                this.on('observable:change:hint', function(e) {
                    this.observableChange(view, e, opt);
                })
            },

//        /**
//         * Событие вызывается если изменяется свойство в объекте ObservableConfig
//         *
//         * @param e
//         * @param notJustChanged -
//         *            should pass true only first time when view was rendered
//         */
            observableChange : function(view, e, opt, notJustChanged) {
                var model = this, notJustChanged = notJustChanged || false;

                if (!model && e.sender) {
                    model = e.sender._model || e.sender.source._model;
                }

                // TODO: when you come back to previous step model is false
                if (!model || _.isEmpty(model.hints))
                    return;

//            /** Применение функции только на изменившийся атрибут модели * */
                var changedAttrs = model.changedAttributes() || {};

                // т.к. изменилось значение свойства модели , нужно пересчитать подсказки
                _.each(model.hints, function(hintItem, field) {
                    var isChanged = (field in changedAttrs || notJustChanged);

//                /** Правила при которых будет выполнятся функция отображения подсказки после рендеринга вьюхи* */
                    var rules = [ 'always', 'always_skip_errors', 'after_change_anything' ];

//                /**
//                 * Правила при которых будет выполнятся функция отображения подсказки после изменения свойства модели
//                 * kendo.Observable *
//                 */
                    var RENDER_RULES = [];
                    if (!notJustChanged) {
                        if (isChanged) {
                            RENDER_RULES = [ 'after_change', 'after_change_always' ];
                        }
                    } else {
                        RENDER_RULES = [ 'once_render' ];
                    }

                    rules = rules.concat(RENDER_RULES);

                    model.hintHub({
                        name : field,
                        rules : rules,
                        fn : opt.showHint,
                        fnClear : opt.hideHint,
                        view : view,
                        isChanged : isChanged
                    });
                });

                model.handleHintBlocks(view, opt);
            },

//        /**
//         * Обработчик вывода блока подсказок с контентом Пример использования на шаге: Работа и доход Определение
//         * поведения в модели JobAndIncome: currentJobPlace currentJobPlace - имя атрибута соответсвует {id | className}
//         * поля относительно которого будет вставлен контент
//         *
//         * @param view
//         * @param opt
//         */
            handleHintBlocks : function(view, opt) {
                var model = this;
//            /** Вывод на форме блоков с подсказками если определены * */
                if (!_.isUndefined(model.hints.options) && !_.isUndefined(model.hints.options.blocks)) {
                    _.each(model.hints.options.blocks, function(itemOpt, elementId) {
                        if (itemOpt.show) {
                            opt.showBlock.apply(view, [ elementId, itemOpt ]);
                        } else {
                            opt.clearBlock.apply(view, [ elementId, itemOpt ]);
                        }
                    });
                }
            },

//        /**
//         * Точка обработки правил появления/скрытия подсказки
//         *
//         * @param hintOptions - {
//         *            fieldName: String, rules: Array, fn: function which should be run if rules passed successful }
//         */
            hintHub : function(opt) {
                var model = this, view = opt.view, element = opt.element, fieldName = element ? element.prop('name')
                    : opt.name, rules = opt.rules, fn = opt.fn, fnClear = opt.fnClear;

                if (!_.isEmpty(model.hints)) {
                    if (fieldName in model.hints) {
                        var hint = _.clone(model.hints[fieldName]);
                        var defaultDefine = !_.isUndefined(hint['default']);

//                    /** Если нужно применяем default values для hint-а взятого у модели* */
                        _.each([ 'text', 'rule' ], function(field) {
                            if (_.isUndefined(hint[field]) && defaultDefine) {
                                if (!_.isUndefined(hint['default'][field])) {
                                    hint[field] = hint['default'][field];
                                }
                            }
                        });

                        if (!_.isUndefined(hint.fn) && _.isFunction(hint.fn)) {
                            var fnHintResult = hint.fn.apply(model);
                            if (!_.isUndefined(fnHintResult)) {
                                hint = _.extend(hint, fnHintResult);
                            }
                        }

                        if (!_.isUndefined(hint.fnChange) && _.isFunction(hint.fnChange) && opt.isChanged) {
//                        /***********************************************************************************************
//                         * Вызов функции логики расчета подсказки в контексте полной модели, а не только внутреннего
//                         * объекта hint
//                         **********************************************************************************************/
                            var hintAfterChange = hint.fnChange.apply(model);
                            if (!_.isUndefined(hintAfterChange)) {
                                hint = hintAfterChange;
                            }
                        }

                        hint.rule = !_.isUndefined(hint.rule) ? hint.rule : '';

                        if (_.contains(rules, hint.rule) && !_.str.isBlank(hint.text)) {
//                        /** Применение метода обрабатывающего подсказку в DOM структуре. Реализуется во View * */
                            fn.apply(view, [ view, fieldName, hint.text, BackboneHints.opt.hintSelector ]);
                        } else {
                            if (!_.isUndefined(fnClear) && !_.isUndefined(hint.rule)
                                && !_.contains([ 'not_update' ], hint.rule)) {
//                            /** Применение метода очистки DOM element от подсказки * */
                                fnClear.apply(view, [ view, fieldName, hint.text, BackboneHints.opt.hintSelector ]);
                            }
                        }
                    }
                }
            }
        }
   };
        /** ******** ************* */
        return BackboneHints;
    }
));