(function (d) {
    d.fn.extend({initMoneyField: function () {
        if (!d.event._moneyFieldCache) {
            d.event._moneyFieldCache = [];
        }
        return this.each(function () {
            if (!this._moneyFieldId) {
                this._moneyFieldId = d.event.guid++;
                d.event._moneyFieldCache[this._moneyFieldId] = new c(this);
            }
            var e = d.event._moneyFieldCache[this._moneyFieldId];
            e.init();
        });
    }, setMoneyValue: function (e) {
        a.call(this, "_setMoneyValue", e);
    }, setPrecision: function (e) {
        a.call(this, "_setPrecision", e);
    }, setValidMoneyRegexp: function (e) {
        a.call(this, "_setValidMoneyRegexp", e);
    }, setFullMoneyRegexp: function (e) {
        a.call(this, "_setFullMoneyRegexp", e);
    }, setIntegerMoneyRegexp: function (e) {
        a.call(this, "_setIntegerMoneyRegexp", e);
    }, setDelimiterChar: function (e) {
        a.call(this, "_setDelimiterChar", e);
    }});
    var a = function (g, i, h, f, e) {
        return this.each(function () {
            var j = b(this);
            if (j) {
                j[g](i, h, f, e);
            }
        });
    };

    function c(e) {
        this.elem = e, this._MONEY_FORMAT_REGEXP = /(\d{1,3}(?=(\d{3})+(?:(\.|,)\d|\b)))/g, this._VALID_MONEY_REGEXP = /^-?\d*((\.|,)\d{0,2})?$/, this._FULL_MONEY_REGEXP = /^-?\d+(\.|,)\d{2}$/, this._INTEGER_MONEY_REGEXP = /^-?\d+$/, this._DELIMETER_REGEXP = /(\.|,)/, this._DELIMITER_CHAR = " ";
        this.precision = 2;
        d.extend(c.prototype, {init: function () {
            var f = this;
            d(this.elem).keyup(function (g) {
                return f.keyUpHandler(g);
            });
            d(this.elem).keydown(function (g) {
                return f.keyDownHandler(g);
            });
        }, _setPrecision: function (f) {
            this.precision = f;
            this._VALID_MONEY_REGEXP = new RegExp("^-?\\d*((\\.|,)\\d{0," + f + "})?$");
            this._FULL_MONEY_REGEXP = new RegExp("^-?\\d+(\\.|,)\\d{" + f + "}?$");
        }, _setValidMoneyRegexp: function (f) {
            this._VALID_MONEY_REGEXP = f;
        }, _setFullMoneyRegexp: function (f) {
            this._FULL_MONEY_REGEXP = f;
        }, _setIntegerMoneyRegexp: function (f) {
            this._INTEGER_MONEY_REGEXP = f;
        }, _setDelimiterChar: function (f) {
            this._DELIMITER_CHAR = f;
        }, _setMoneyValue: function (f) {
            var g = typeof f == "number" ? f.toString() : f;
            var h = getStringWithoutSpace(g);
            if (this._VALID_MONEY_REGEXP.test(h)) {
                g = h.replace(this._MONEY_FORMAT_REGEXP, "$1" + this._DELIMITER_CHAR);
            }
            d(this.elem).val(g);
        }, updateMoneyField: function (g) {
            var i = false;
            var k = this;
            var f = function (u) {
                var t = function (v) {
                    return k._VALID_MONEY_REGEXP.test(v);
                };
                var s = getStringWithoutSpace(u);
                if (t(s)) {
                    return s.replace(k._MONEY_FORMAT_REGEXP, "$1" + k._DELIMITER_CHAR);
                } else {
                    try {
                        var p = parseFloat(s);
                        var q = parseFloat(Math.floor(p * Math.pow(10, k.precision)) / Math.pow(10, k.precision)).toFixed(k.precision) + "";
                        if (t(q)) {
                            return q.replace(k._MONEY_FORMAT_REGEXP, "$1" + k._DELIMITER_CHAR);
                        }
                    } catch (r) {
                    }
                }
                i = true;
                return g.preventValue;
            };
            var o = function (p) {
                return p.split(" ").length - 1;
            };
            var n = g.value;
            var l = getStringWithoutSpace(n);
            if (g.maxDigitCount != undefined) {
                var h = Math.max(0, l.length - g.maxDigitCount);
                if (h != 0) {
                    l = l.substr(0, g.maxDigitCount);
                }
            }
            var j = f(l);
            var m = getCaretPosition(g)[0];
            m += o(j.substr(0, m)) - o(n.substr(0, m));
            if (i) {
                m = g.preventPosition[0];
            }
            g.value = j;
            setCaretPosition(g, m);
            if (d('body').hasClass('lt-ie9')) {
            	d(g).trigger('change');
            }
        }, keyDownHandler: function (j) {
            var h = getEventTarget(j);
            var k = h.value;
            var i = getStringWithoutSpace(k);
            if (this._VALID_MONEY_REGEXP.test(i)) {
                h.preventValue = k;
                h.preventPosition = getCaretPosition(h);
            }
            if (j.keyCode == 13) {
                this.updateMoneyField(h);
                return true;
            }
            var l = h.preventPosition;
            if (j.keyCode == 8 && l[0] - 1 > 0 && k.split("")[l[0] - 1] == this._DELIMITER_CHAR) {
                setCaretPosition(h, l[0] - 1);
                return true;
            }
            if (j.keyCode == 46 && l[0] == l[1] && l[0] < k.length && k.split("")[l[0]] == this._DELIMITER_CHAR) {
                setCaretPosition(h, l[0] + 1);
                return true;
            }
            if (0 <= j.keyCode && j.keyCode < 45 || j.keyCode == 8 || j.keyCode == 46 || j.keyCode >= 112 && j.keyCode <= 123 || j.ctrlKey && j.keyCode == 88) {
                return true;
            }
            var g = i.length;
            var f = h.maxDigitCount == undefined || g < h.maxDigitCount;
            if (g > 0 && j.keyCode == 109) {
                return false;
            }
            if (f && k.length - l[1] <= this.precision && (j.keyCode == 110 || j.keyCode == 188 || j.keyCode == 190 || j.keyCode == 191)) {
                return this._INTEGER_MONEY_REGEXP.test(i);
            }
            if (!(j.keyCode >= 48 && j.keyCode <= 57 || j.keyCode >= 96 && j.keyCode <= 105 || j.ctrlKey && j.keyCode == 86 || j.ctrlKey && j.keyCode == 45 || j.shiftKey && j.keyCode == 45 || j.ctrlKey && j.keyCode == 67 || j.ctrlKey && j.keyCode == 65)) {
                return false;
            }
            if (f && this._FULL_MONEY_REGEXP.test(i)) {
                return l[0] <= k.search(this._DELIMETER_REGEXP) || l[0] != l[1];
            }
            return f || l[0] != l[1] && !(j.ctrlKey && j.keyCode == 86);
        }, keyUpHandler: function (f) {
            if (f.ctrlKey && f.keyCode != 86 && f.keyCode != 88 || f.keyCode != 8 && 0 < f.keyCode && f.keyCode < 45 && f.keyCode != 32) {
                return true;
            }
            this.updateMoneyField(getEventTarget(f));
            return true;
        }});
    }

    function b(e) {
        if (e._moneyFieldId) {
            return d.event._moneyFieldCache[e._moneyFieldId];
        }
        return false;
    }

    function getEventTarget(b) {
        var a;
        if (!b) {
            var b = window.event;
        }
        if (b.target) {
            a = b.target;
        } else {
            if (b.srcElement) {
                a = b.srcElement;
            }
        }
        if (a.nodeType == 3) {
            a = a.parentNode;
        }
        return a;
    }

    function getStringWithoutSpace(a) {
        return a.replace(/ /g, "");
    }

    function getCaretPosition(a) {
        try {
            if (document.selection) {
                var b = document.selection.createRange();
                return[-b.moveStart("character", -a.value.length), -b.moveEnd("character", -a.value.length)];
            }
            if (a.selectionStart || a.selectionStart == "0") {
                return[a.selectionStart, a.selectionEnd];
            }
        } catch (c) {
        }
        return[0, 0];
    }

    function setCaretPosition(b, e, d) {
        try {
            if (b.setSelectionRange) {
                b.setSelectionRange(e, d != null ? d : e);
            } else {
                if (b.createTextRange) {
                    var a = b.createTextRange();
                    a.collapse(true);
                    a.moveEnd("character", d != null ? d : e);
                    a.moveStart("character", e);
                    a.select();
                }
            }
        } catch (c) {
        }
    }
})(jQuery);