/*!
* jQuery resizer plugin 
*/

;(function ($, window, document, undefined ) {
    
    var pluginName = 'resizer',
        defaults = {
            cssType: {
                'aside': {
                    position: 'absolute',
                    width: '10px',
                    top: 0,
                    bottom: 0,
                    right: 0,
                    cursor: 'col-resize',
                    'z-index': 100,
                    'background-image': 'url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAANCAYAAABlyXS1AAAALElEQVQYlWNgIBcwwhjz5sz7' + 
                        '//vvb2NWZtazv//+Nk5PTz/HhE/nMJIkHwAAK/IMEtcq6OYAAAAASUVORK5CYII=")',
                    'background-repeat': 'no-repeat',                
                    'background-position': '50% 50%'                
                },
                'both-sides': {
                    position: 'absolute',
                    width: '7px',
                    height: '7px',
                    bottom: '1px',
                    right: '1px',
                    cursor: 'se-resize',
                    'background-image': 'url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHAgMAAAC5PL9AAAAACVBM' + 
                        'VEUAAAAkIiQkIiQv+qYgAAAAAnRSTlMAGGX/VW4AAAAdSURBVAjXY2CQYGBIYWCcwcCWwiA5gyEtgWFmAwAeLQQQ6gO2ZAAAAABJRU5ErkJggg==")',
                    'background-repeat': 'no-repeat',
                    'z-index': 100             
                },
            }
        };

    function Resizer(el, params) {
        this.params = $.extend( {}, defaults, params) ;
        this._defaults = defaults;
        this._name = pluginName;
        
        this.$el  = $(el);
        this.type = params.type || 'aside';
        this.$container = params.container || null;
        this.$triggerWrapper = params.triggerWrapper || this.$el;
        this.heightRegEls = params.heightRegEls || null;

        this.$trigger = $('<div class="js-resize-' + this.type + '-trigger"></div>');
        this.$trigger.css(this._defaults['cssType'][this.type]);
        this.$triggerWrapper.append(this.$trigger);

        this.elWidth = this.$el.outerWidth();

        this.triggerMousedownBoth = false;
        this.triggerMousedownAside = false;

        this.oldCoordX = null;

        document.onselectstart = function() {
            return false;
        };

        this.$trigger.on('mouseup', $.proxy(function() {
            if ( this.type == 'aside' ) this.triggerMousedownAside = false;
            else if ( this.type == 'both-sides' ) this.triggerMousedownBoth = false;
        }, this));

        $(document).on('mouseup', $.proxy(function() {
            if ( this.type == 'aside' ) this.triggerMousedownAside = false;
            else if ( this.type == 'both-sides' ) this.triggerMousedownBoth = false;
        }, this));

        this.mousedown = function(e) {
            this.elWidth = this.$el.outerWidth();
            this.pageX = e.pageX;
            if ( this.type == 'aside' ) this.triggerMousedownAside = true;
            else if ( this.type == 'both-sides' ) this.triggerMousedownBoth = true;
        };

        this.getDirection = function(e) {
            var direction;
            if ( !this.oldCoordX ) {
                this.oldCoordX = e.pageX;
                direction = 'stop';
            }

            if ( this.oldCoordX - e.pageX < 0 ) direction = 'right';
            else if ( this.oldCoordX - e.pageX > 0 ) direction = 'left';

            this.oldCoordX = e.pageX;

            return direction;
        }
    }

    var pluginFuncs = {
        'both-sides': function (el, params) {
            Resizer.apply(this, arguments);
            $(document).on('mousemove', $.proxy(function(e) {
                if ( this.triggerMousedownBoth ) {
                    this.$el.css('width', this.elWidth + (e.pageX - this.pageX));
                    for ( var i = 0, len = this.heightRegEls.length; i < len; i++ ) {
                        this.heightRegEls[i].css('height', this.heightRegElsValues[i] + (e.pageY - this.pageY));
                    }
                }
            }, this) );
            this.$trigger.on('mousedown', $.proxy(function(e) {
                this.mousedown(e);
                if ( this.heightRegEls ) {
                    this.heightRegElsValues = [];
                    for ( var i = 0, len = this.heightRegEls.length; i < len; i++ ) {
                        this.heightRegElsValues.push(this.heightRegEls[i].outerHeight());
                    }
                    this.pageY = e.pageY;
                }
            }, this));
        },
        'aside': function (el, params) {
            Resizer.apply(this, arguments);
            
            $(document).on('mousemove', $.proxy(function(e) {
                if ( this.triggerMousedownAside ) {
                    if ( +this.$el[0].style.width.slice(0, -2) >= this.asideMaxWidth ) {
                        if ( (this.getDirection(e) == 'left') && (e.pageX <= (this.$el[0].getBoundingClientRect().right + window.pageXOffset)) ) {
                            this.$el.css('width', this.elWidth + (e.pageX - this.pageX));
                        } else {
                            this.$el[0].style.width = this.asideMaxWidth + 'px';
                            return false;
                        }
                    }
                    else {
                        this.$el.css('width', this.elWidth + (e.pageX - this.pageX));
                    }

                }
            }, this) );
            this.$trigger.on('mousedown', $.proxy(function(e) {
                this.mousedown(e);
                if (this.$container ) {
                    this.containerWidth = this.$container.outerWidth();
                    this.asideMaxWidth = Math.ceil(this.containerWidth * 0.6);

                }
            }, this));            
        }
    }
    
    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            new pluginFuncs[options.type]( this, options );
        });
    }

})( jQuery, window, document );

