'use strict';
require.config({
    baseUrl: './js/',
	shim: {
		'jquery.customSelect': {
        	deps: ['jquery'],
        	exports: '$'
        },
        'jquery.numberFormat': {
        	deps: ['jquery'],
        	exports: '$'
        },
        'bootstrap': {
        	deps: ['jquery'],
        	exports: '$'
        }
    },
	
    paths: {
        jquery: 'libs/jquery/jquery-1.11.2.min',
        bootstrap: 'libs/bootstrap/bootstrap',
        backbone: 'libs/backbone/backbone',
        underscore: 'libs/underscore/underscore-1.8.2',
       'underscore.string': 'libs/underscore/underscore.string',

        /* alias all backbone extensions */
        'backbone.marionette': 'libs/backbone/backbone.marionette',
        'backbone.wreqr': 'libs/backbone/backbone.wreqr',
        'backbone.babysitter': 'libs/backbone/backbone.babysitter',
        'backbone.paginator': 'libs/backbone/backbone.paginator',
        'backbone.validation': 'libs/backbone/backbone-validation-amd',
        'backbone.marionette.modals': 'libs/backbone/backbone.marionette.modals',
        'backbone.stickit': 'libs/backbone/backbone.stickit',
        
        
        /* jquery plugins */
        'jquery.customSelect': 'libs/jquery/jquery.customSelect',
        'jquery.numberFormat': 'libs/jquery/numberFormat',
        'jquery.ui': 'libs/jquery/jquery-ui-1.10.3.min',
        'jquery.resizer': 'libs/jquery/jquery.resizer',
     
      
    
        
        tmpl: '../templates',
        
        handlebars: 'libs/require-handlebars-plugin/hbs/handlebars',
        
        i18nprecompile: 'libs/require-handlebars-plugin/hbs/i18nprecompile',
        json2 : 'libs/require-handlebars-plugin/hbs/json2',
        hbs: 'libs/require-handlebars-plugin/hbs2',
     
    },
    
    hbs: {
    	disableI18n: true,
    	templateExtension: 'html'
    }
});