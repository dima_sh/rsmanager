package ru.sbtsbol.admin.rsmanager.service;

import ru.sbtsbol.admin.rsmanager.model.Resource;


import java.awt.image.BufferedImage;
import java.io.IOException;


/**
 * Created with IntelliJ IDEA. User: SBT-Konovalov-EI Date: 21.05.15 Time: 13:21
 * To change this template use File | Settings | File Templates.
 */
public interface ImageService {

  BufferedImage getTumb(String path, Resource resource) throws IOException;

}
