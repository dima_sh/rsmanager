package ru.sbtsbol.admin.rsmanager.filter;

/**
 * Created with IntelliJ IDEA. User: SBT-Konovalov-EI Date: 19.05.15 Time: 17:14
 * To change this template use File | Settings | File Templates.
 */
public class FilterException extends Exception {

  public FilterException() {
    super();
  }

  public FilterException(String message, Throwable cause) {
    super(message, cause);
  }

  public FilterException(String message) {
    super(message);
  }

  public FilterException(Throwable cause) {
    super(cause);
  }

}
