package ru.sbtsbol.admin.rsmanager.filter;

import java.io.InputStream;
import java.util.List;

/**
 * Created with IntelliJ IDEA. User: SBT-Konovalov-EI Date: 20.05.15 Time: 19:28
 * To change this template use File | Settings | File Templates.
 */
public class FilterManager implements Filter {

  private List<Filter> filterList;

  @Override
  public boolean isReadable(String url) {
    for (Filter filter : filterList) {
      if (!filter.isReadable(url)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean isWriteable(String url) {
    for (Filter filter : filterList) {
      if (!filter.isWriteable(url)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean isLocked(String url) {
    for (Filter filter : filterList) {
      if (filter.isLocked(url)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean canCreate(byte[] content, String url) {
    for (Filter filter : filterList) {
      if (!filter.canCreate(content, url)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean canCreate(InputStream is, String url) {
    for (Filter filter : filterList) {
      if (!filter.canCreate(is, url)) {
        return false;
      }
    }
    return true;
  }

  public List<Filter> filterList() {
    return filterList;
  }

  public void setFilterList(List<Filter> filterList) {
    this.filterList = filterList;
  }

}
