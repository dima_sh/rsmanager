package ru.sbtsbol.admin.rsmanager.service;

import ru.sbtsbol.admin.rsmanager.error.RsManagerException;
import ru.sbtsbol.admin.rsmanager.model.Resource;


import java.io.IOException;
import java.io.InputStream;
import java.util.List;


/**
 * Resource Manager interface.
 * 
 * @author SBT-Shiryaev-DmV
 * 
 */
public interface ResourceManager {
  /**
   * get Resource list form the directory.
   * 
   * @param url
   *          - path to resource
   * @return list of resources
   * @throws RsManagerException
   *           when something wrong
   */
  List<Resource> listResources(String url) throws RsManagerException;

  /**
   * Get resource by url.
   * 
   * @param url
   *          - path to Resource
   * @return founded Resource
   * @throws RsManagerException
   *           while getting Resource
   */
  Resource findResource(String url) throws RsManagerException;

  /**
   * Create file by byte[] content.
   * 
   * @param fileContent
   *          - file content as byte[]
   * @param fileName
   *          - fileName
   * @param resourceUrl
   *          - url name
   * @return {@link Resource}
   * @throws IOException
   *           - when error while creating
   */
  Resource createFile(byte[] fileContent, String url) throws RsManagerException;

  /**
   * Create file by InputStream.
   * 
   * @param is
   *          inputStream
   * @param fileName
   *          - fileName
   * @param resourceUrl
   *          - url name
   * @return {@link Resource}
   * @throws IOException
   *           - when error while creating
   */
  Resource createFile(InputStream is, String url) throws RsManagerException;

  /**
   * Operation delete.
   * 
   * @param url
   *          - resource url
   * @return - deleted resource information
   * @throws IOException
   *           - error while deleting
   */
  Resource deleteResource(String url) throws RsManagerException;

  /**
   * Rename Resource.
   * 
   * @param sourceUrl
   *          - resource path
   * @param newName
   *          - new name of resource
   * @return renamed resource
   * @throws RsManagerException
   *           - error while renaming
   */
  Resource renameResource(String sourceUrl, String newName) throws RsManagerException;

  /**
   * rename, copy, replace, operation.
   * 
   * @param url
   *          - old url
   * @param newUrl
   *          - new Url
   * @param mustToDeleteOld
   *          - <code>true</code> - replace operation, <code>false</code> -
   *          copy, rename operations
   * @return new Resource link
   * @throws IOException
   *           - when something wrong
   */
  Resource replaceResource(String url, String newUrl, boolean mustToDeleteOld)
      throws RsManagerException;

  /**
   * Make directory resource.
   * 
   * @param parentUrl
   *          - parent Url
   * @param name
   *          - name of created dir
   * @return created Resource {@see Resource}
   * @throws IOException
   *           - error when creating
   */
  Resource mkDir(String url) throws RsManagerException;

  /**
   * Get alias name of object for example (D, Disk1).
   * 
   * @return rootName
   */
  String getRootName();

  /**
   * Get resource body.
   * 
   * @param url
   *          - resource path
   * @return {@see InputStream}
   * @throws RsManagerException
   *           error when get resource body
   */
  InputStream getInputStream(String url) throws RsManagerException;

  List<Resource> searchResource(String searhcName, String parentUrl) throws RsManagerException;

}
