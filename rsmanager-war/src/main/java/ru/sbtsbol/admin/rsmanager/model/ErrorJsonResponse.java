package ru.sbtsbol.admin.rsmanager.model;


/**
 * Json Error presentation.
 * 
 * @author SBT-Shiryaev-DmV
 * 
 */
public class ErrorJsonResponse {

  /** Code of error. */
  private String code;
  /** ErrorType. */
  private String details = "";

  /**
   * Constructor.
   * 
   * @param code - error code
   * @param details - details message
   */
  public ErrorJsonResponse(String code, String details) {
    super();
    this.code = code;
    this.details = details;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getDetails() {
    return details;
  }

  public void setDetails(String details) {
    this.details = details;
  }

  @Override
  public String toString() {
    return "ErrorJsonResponse [code=" + code + ", details=" + details + "]";
  }

}
