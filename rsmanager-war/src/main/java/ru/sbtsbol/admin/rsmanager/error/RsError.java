package ru.sbtsbol.admin.rsmanager.error;

import ru.sbtsbol.admin.rsmanager.model.ErrorJsonResponse;

/**
 * Error Types. 
 * @author SBT-Shiryaev-DV
 *
 */
public enum RsError {

  UNKNOWN(0), DELETEFAIL(1), CREATE(2), RENAME(3), CUTPASTE(4), COPYPASTE(5), 
  UPDATEBADREQUEST(6), UNSUPPORTEDOPERATION(7), SEARCH(8), LIST(9), GETRESOURCE(10),
  DELETEDENY(11), CREATDENY(12), RENAMEEXIST(13), RENAMEFAIL(14), 
  RENAMEDENY(15), VOLUMEGET(16), VOLUMEFIND(16), NODIRCONTENT(17), 
  GETCONTENTDENY(18), GETCONTENT(19), REPLACEFAIL(20), REPLACEDENY(21);

  private int code;

  private RsError(int code) {
    this.code = code;
  }

  public int getCode() {
    return code;
  }
  /**
   * RsManager Exception generator.
   * @param exception - caused error
   * @return {@link RsManagerException}
   */
  public RsManagerException generateRsException(Exception exception) {
    RsManagerException ex = new RsManagerException(exception);
    ex.setErrorCode(name());
    return ex;
  }
  /**
   * RsManager Exception generator.
   * @param message - error message
   * @return {@link RsManagerException}
   */
  public RsManagerException generateRsException(String message) {
    RsManagerException ex = new RsManagerException(message);
    ex.setErrorCode(name());
    return ex;
  }
  /**
   * Generate JsonResponse object.
   * @param details - error message details
   * @return {@link ErrorJsonResponse}
   */
  public ErrorJsonResponse generateJsonResponse(String details) {
    ErrorJsonResponse errorJsonResponse = new ErrorJsonResponse(name(), details);
    return errorJsonResponse;
  }

}
