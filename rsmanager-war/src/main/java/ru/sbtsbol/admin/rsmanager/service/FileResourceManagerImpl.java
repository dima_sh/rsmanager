package ru.sbtsbol.admin.rsmanager.service;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import ru.sbtsbol.admin.rsmanager.error.RsError;
import ru.sbtsbol.admin.rsmanager.error.RsManagerException;
import ru.sbtsbol.admin.rsmanager.filter.AllowAllFilter;
import ru.sbtsbol.admin.rsmanager.filter.FilterException;
import ru.sbtsbol.admin.rsmanager.model.FileResource;
import ru.sbtsbol.admin.rsmanager.model.Resource;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * ResourceManager implementation based on file system.
 * 
 * @author SBT-Shiryaev-DmV
 * 
 */
@Service
public class FileResourceManagerImpl extends AbstractFileResourceManager {

  private static final Logger logger = LoggerFactory.getLogger(FileResourceManagerImpl.class);
  /**
   * Constructor.
   */
  public FileResourceManagerImpl() {
    this.filterManager = new AllowAllFilter();
    logger.info("Construct FileResourceManagerImpl with security policy [{}]", 
        this.getFilterManager().getClass().getName());
  }

  @Override
  public List<Resource> listResources(String path) throws RsManagerException {
    if (StringUtils.isEmpty(path)) {
      path = rootName;
    }
    logger.info("Getting resources by path {} ", path);
    if (!path.endsWith("\\")) {
      path = path + "\\";
    }
    String realPath = rootPath + "\\" + path.substring((rootName + "\\").length());
    logger.info("Getting resources: normalizedPath is {} ", path);
    File root = new File(realPath);
    File[] files = root.listFiles();
    List<Resource> resources = new ArrayList<Resource>();
    if (files != null) {
      for (File file : files) {
        FileResource res = new FileResource(file, rootPath, rootName);
        setSecutiryField(res);
        logger.debug("Returning Resource [{}]", res.toString());;
        resources.add(res);
      }
    }
    logger.info("Getting resources: got [{}] children by path {} ", resources.size(), path);
    return resources;
  }
  
  /**
   * Rename resource.
   * @sourceUrl - path of renamed source.
   * @newName - new Name of resource.
   */
  public Resource renameResource(String sourceUrl, String newName) throws RsManagerException {
    logger.info("Renaming resource with path {} into newname {} ", sourceUrl, newName);
    FileResource resource = getResource(sourceUrl);
    FileResource newResource = getResource(resource.getParentPath() + "\\" + newName);
    if (newResource.getFile().exists()) {
      throw RsError.RENAMEEXIST.generateRsException("The resource already exists");
    }
    try {
      if (resource.getFile().isDirectory()) {
        copyDirectoryRecoursively(resource, newResource);
      } else {
        copyFile(resource, newResource);
      }
    } catch (IOException e) {
      throw RsError.RENAMEFAIL.generateRsException(e);
    } catch (FilterException fe) {
      throw RsError.RENAMEDENY.generateRsException("");
    }
    try {
      logger.info("Renaming: Created new resource, try to delete old resource by path {}",
          resource.getAbsolutePath());
      deleteResource(resource.getAbsolutePath());
    } catch (Exception e) {
      logger.warn("Renaming: Can't delete old resource by path {}   ", resource.getAbsolutePath());
      try {
        logger.warn("Renaming: Can't delete old resource, try rollback renaming ");
        deleteResource(newResource.getAbsolutePath());
      } catch (Exception e1) {
        logger.warn("Can't rollbak rename operation on resource {}", newResource.getAbsolutePath());
      }
      throw RsError.RENAMEFAIL.generateRsException(e);
    }
    return newResource;
  }

  @Override
  public Resource replaceResource(String source, String parentTarget, boolean mustToDelete)
      throws RsManagerException {
    logger.info("Begin replacing from [{}[ to [{}] with delete flag {}   ", 
        source, parentTarget, mustToDelete);
    FileResource resource = getResource(source);
    FileResource parent = getResource(parentTarget);
    if (!parent.getFile().isDirectory()) {
      throw RsError.REPLACEFAIL.generateRsException(
          String.format("Parent [%s] is not directory", parentTarget));
    }
    if (!resource.getFile().exists()) {
      throw RsError.REPLACEFAIL.generateRsException(String.format(
          "Source [%s] not exist or not directory", source));
    }
    FileResource newResource = getResource(parent.getAbsolutePath() + "\\" + resource.getName());
    try {
      if (resource.getFile().isDirectory()) {
        copyDirectoryRecoursively(resource, newResource);
      } else {
        copyFile(resource, newResource);
      }
      logger.info("Replaced from [{}[ to [{}] with delete flag {} succesfully", 
          source, parentTarget, mustToDelete);
    } catch (IOException e) {
      throw RsError.REPLACEFAIL.generateRsException(e);
    } catch (FilterException fe) {
      throw RsError.REPLACEDENY.generateRsException("No permissions");
    }

    if (mustToDelete) {
      try {
        deleteResource(resource.getAbsolutePath());
      } catch (Exception e) {
        logger.warn("Can't delete old resource {}", resource.getAbsolutePath());
      }
    }
    return newResource;
  }

  @Override
  public Resource createFile(byte[] fileContent, String url) throws RsManagerException {
    logger.info("Creating resource by bytesize  with url [{}]", fileContent.length, url);
    return createFile(new BufferedInputStream(new ByteArrayInputStream(fileContent)), url);
  }
  
  @Override
  public Resource createFile(InputStream is, String url) throws RsManagerException {
    logger.info("Creating resource inputstream  with url [{}]",  url);
    FileResource resource = getResource(url);
    try {
      createFile(is, resource);
      logger.info("Created resource inputstream  with url [{}] succesfully",  url);
    } catch (IOException e) {
      throw RsError.CREATE.generateRsException(e);
    } catch (FilterException e) {
      throw RsError.CREATDENY.generateRsException("");
    }
    return resource;
  }

  
  @Caching(evict = { @CacheEvict(value = "cAdmin", key = "#url"),
      @CacheEvict(value = "cAdmin", key = "80 + #url") })
  @Override
  public Resource deleteResource(String url) throws RsManagerException {
    logger.info("Begin deleting resource  with url [{}]",  url);
    FileResource resource = getResource(url);
    if (resource.isRoot()) {
      throw RsError.DELETEFAIL.generateRsException("Can't delete root dir");
    }
    try {
      if (resource.getFile().isDirectory()) {
        deleteDirRecoursively(resource);
      } else {
        deleteFile(resource);
      }
      logger.info("Deleted resource  with url [{}] succesfully",  url);
    } catch (IOException ioe) {
      throw RsError.DELETEFAIL.generateRsException(ioe);
    } catch (FilterException fie) {
      throw RsError.DELETEDENY.generateRsException(fie);
    }
    return resource;
  }

 

  @Override
  public Resource mkDir(String url) throws RsManagerException {
    logger.info("Try making new dir with url [{}]",  url);
    FileResource resource = getResource(url);
    try {
      createDir(resource);
      logger.info("Maked new dir with url [{}] succesfully",  url);
    } catch (FilterException e) {
      throw RsError.CREATDENY.generateRsException("resources.error.mkdirdeny");
    } catch (IOException e) {
      throw RsError.CREATE.generateRsException("resources.error.mkdir");
    }
    return resource;
  }

  @Override
  public List<Resource> searchResource(final String searchName, String parentUrl)
      throws RsManagerException {
    logger.info("Searching resources in parentUrl [{}] by mask", parentUrl,  searchName);
    FileResource resource = getResource(parentUrl);

    Collection<?> files = FileUtils.listFilesAndDirs(resource.getFile(), new IOFileFilter() {

      @Override
      public boolean accept(File file, String arg1) {
        return file.getName().indexOf(searchName) > -1;
      }

      @Override
      public boolean accept(File file) {
        return file.getName().indexOf(searchName) > -1;
      }
    }, new IOFileFilter() {

      @Override
      public boolean accept(File file, String arg1) {
        return file.getName().indexOf(searchName) > -1;
      }

      @Override
      public boolean accept(File file) {
        return file.getName().indexOf(searchName) > -1;
      }
    }

    );

    List<Resource> resources = new ArrayList<Resource>();
    for (Object file : files) {
      if (file.toString().equals(rootPath)) {
        continue;
      }
      File resourceFIle = (File) file;
      resources.add(new FileResource(resourceFIle, rootPath, rootName));
    }
    logger.info("Founded {} resources in parentUrl [{}] by mask", resources.size(), parentUrl,  searchName);
    return resources;
  }

  @Override
  public boolean equals(Object object) {
    if (getClass() != object.getClass()) {
      return false;
    } 
    return ((FileResourceManagerImpl) object).getRootName().equals(this.getRootName());
  }

  @Override
  public InputStream getInputStream(String url) throws RsManagerException {
    logger.info("Get content for resource {}", url);
    Resource resource = getResource(url);
    if (Resource.DIRECTORY_MIME.equals(resource.getMimeTypes())) {
      throw RsError.NODIRCONTENT.generateRsException("This is directory");
    }
    try {
      return getInputStream(resource);
    } catch (IOException e) {
      throw RsError.GETCONTENT.generateRsException(e);
    } catch (FilterException fioe) {
      throw RsError.GETCONTENTDENY.generateRsException(fioe);
    }
  }

/*  public static void main(String[] args) throws Exception {
    System.out.println(System.getProperty("java.io.tmpdir"));
    try {
      FileResourceManagerImpl fr = new FileResourceManagerImpl();
      fr.setRootName("me");
      fr.setRootPath("c:\\TEMP\\TMP");
      Resource ds = fr.deleteResource("me\\3");
      System.out.println(ds);
      System.out.println(((FileResource) ds).getFile().exists());
      System.exit(0);

      List<Resource> ress = fr.listResources("me");
      for (Resource r : ress) {
        System.out.println(r);
      }

      ress = fr.listResources("me\\1\\2");
      for (Resource r : ress) {
        System.out.println(r);
      }
      Resource res = fr.getResource("me\\1\\2");
      System.out.println("-----");
      List<FileResource> ress1 = fr.listResources((FileResource) res);
      for (Resource r : ress1) {
        System.out.println(r);
      }

      res = fr.getResource("me\\1\\2");
      System.out.println(res.getAbsolutePath());
      System.out.println(((FileResource) res).getFile().getAbsolutePath());

      fr.replaceResource("me\\1", "me\\2", true);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }*/
}
