package ru.sbtsbol.admin.rsmanager.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Json response for many rs manager restfull operations
 * 
 * @author SBT-Shiryaev-DmV.
 *        
 * <p>
 *         <b>Example of json object</b><br>
 *         <code>
 *   {resources{
 *      {{@link Resource}}, {{@link Resource}},{....}
 *   },
 *   warnings {
 *        {@link ErrorJsonResponse}, {....}   
 *   }
 * </code>
 * </p>
 * 
 */
public class JsonResponse {
  /** list of resources. */
  private List<Resource> resources;
  /** list of warning messages. */
  private List<ErrorJsonResponse> warnings;

  /**
   * Add resource to json.
   * 
   * @param resource
   *          - {@link Resource}
   */
  public void addResource(final Resource resource) {
    if (resources == null) {
      resources = new ArrayList<Resource>();
    }
    resources.add(resource);
  }
  /**
   * Add resources to json response.
   * @param resourcecollection - collection of resource
   */
  public void addResources(final List<Resource> resourcecollection) {
    if (resources == null) {
      resources = new ArrayList<Resource>();
    }
    resources.addAll(resourcecollection);
  }
  
  /**
  * Add warnings to response.
  * @param warning - warning object {@link ErrorJsonResponse}
  */
  public void addWarning(final ErrorJsonResponse warning) {
    if (warnings == null) {
      warnings = new ArrayList<ErrorJsonResponse>();
    }
    warnings.add(warning);
  }

  public List<Resource> getResources() {
    return resources;
  }

  public List<ErrorJsonResponse> getWarnings() {
    return warnings;
  }

}
