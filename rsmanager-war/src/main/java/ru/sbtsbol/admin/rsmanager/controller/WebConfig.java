package ru.sbtsbol.admin.rsmanager.controller;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * This config need to ignore dotted and subpath for ajax request.
 * 
 * @author SBT-Shiryaev-DmV
 * 
 */
@EnableWebMvc
@Configuration
@ComponentScan(basePackageClasses = {ResourceManagerController.class})
public class WebConfig extends WebMvcConfigurerAdapter {

  @Override
  public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
    configurer.favorPathExtension(false).favorParameter(false).useJaf(false);
    configurer.defaultContentType(MediaType.APPLICATION_JSON);
  }

}
