package ru.sbtsbol.admin.rsmanager.filter;

/**
 * DenyWrite filter stub.
 * 
 * @author SBT-Shiryaev-DV
 */
public class DenyWriteFilter extends AllowAllFilter implements Filter {

  @Override
  public boolean isWriteable(String url) {
    return false;
  }

}
