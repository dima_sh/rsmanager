package ru.sbtsbol.admin.rsmanager.controller;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import ru.sbtsbol.admin.rsmanager.error.RsError;
import ru.sbtsbol.admin.rsmanager.error.RsManagerException;
import ru.sbtsbol.admin.rsmanager.model.ErrorJsonResponse;
import ru.sbtsbol.admin.rsmanager.model.JsonResponse;
import ru.sbtsbol.admin.rsmanager.model.Resource;
import ru.sbtsbol.admin.rsmanager.service.ResourceManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * Store drivers list and configuration, also standart exception handler of
 * controller.
 * 
 * @author SBT-Shiryaev-DmV
 * 
 */
public class MessageResourceProvider {

  private static final Logger logger = LoggerFactory.getLogger(MessageResourceProvider.class);
  private DriverConfiguration driverConfiguration;

  public MessageResourceProvider(DriverConfiguration driverConfiguration) {
    super();
    this.driverConfiguration = driverConfiguration;
  }
  /**
   * Getting driver by volume Id.
   * @param volumeId - volume id
   * @return {@link ResourceManager}
   * @throws RsManagerException - when driver is not found
   */
  public ResourceManager findByVolumeId(String volumeId) throws RsManagerException {
    if (driverConfiguration.getDrivers().containsKey(volumeId)) {
      return driverConfiguration.getDrivers().get(volumeId);
    } else {
      throw RsError.VOLUMEGET.generateRsException("Can't find driver [" + volumeId + "]");
    }
  }
  /**
   * Getting volume by driver name (alias).
   * @param alias - driver name
   * @return {@link ResourceManager}
   * @throws RsManagerException - when driver is not found
   */
  public ResourceManager findByVolumeAlias(String alias) throws RsManagerException {
    for (Map.Entry<String, ResourceManager> volume : getDrivers().entrySet()) {
      if (volume.getValue().getRootName().equals(alias)) {
        return volume.getValue();
      }
    }
    throw RsError.VOLUMEGET.generateRsException("Can't find driver by name [" + alias + "]");

  }

  public ResourceManager getDefaultVolume() throws RsManagerException {
    return findByVolumeId(driverConfiguration.getDefaultDriver());
  }

  public String getDefaultVolumeId() {
    return driverConfiguration.getDefaultDriver();
  }

  public Map<String, ResourceManager> getDrivers() {
    return driverConfiguration.getDrivers();

  }

  /**
   * Replace Resource from one volume to another volume.
   * 
   * @param sourceVolume
   *          - source Volume
   * @param sourceUrl
   *          - source url
   * @param targetVolume
   *          - target Volume
   * @param targetUrl
   *          - target url
   * @throws IOException
   *           - when something wrong
   */
  protected Resource replace(ResourceManager sourceResource, String sourceUrl,
      ResourceManager targetResource, String targetUrl, boolean mustToDelete)
      throws RsManagerException {

    logger.info("Try to replace resource between to drivers");
    Resource newResource = recursiveCopyFiles(sourceResource, sourceUrl, targetResource, targetUrl);
    logger.info("Resources have succesfully replaced");
    if (mustToDelete) {
      try {
        logger.info("Deleting old resource {} ", sourceUrl);
        sourceResource.deleteResource(sourceUrl);
      } catch (Exception e) {
        logger.error("Error while deleting {}", sourceUrl, e);
      }
    }
    return newResource;
  }

  private Resource recursiveCopyFiles(ResourceManager sourceManager, String sourceUrl,
      ResourceManager targetManager, String targetUrl) throws RsManagerException {
    Resource src = sourceManager.findResource(sourceUrl);

    if ("directory".equals(src.getMimeTypes())) {
      Resource dir = targetManager.mkDir(targetUrl + "\\" + src.getName());
      List<Resource> resources = sourceManager.listResources(sourceUrl);
      for (Resource resource : resources) {
        recursiveCopyFiles(sourceManager, resource.getAbsolutePath(), targetManager,
            dir.getAbsolutePath());
      }
      return dir;
    } else {
      InputStream is = null;
      try {
        is = sourceManager.getInputStream(src.getAbsolutePath());
        return targetManager.createFile(is, targetUrl + "\\" + src.getName());
      } finally {
        IOUtils.closeQuietly(is);
      }
    }
  }

  protected JsonResponse searchResources(String mask) {
    Map<String, ResourceManager> drivers = getDrivers();
    logger.info("Try to search resources by mask [{}] between {} drivers", mask, drivers.size());
    JsonResponse response = new JsonResponse();
    for (Map.Entry<String, ResourceManager> volume : drivers.entrySet()) {
      try {
        response.addResources(volume.getValue().searchResource(mask, ""));
      } catch (Exception e) {
        logger.warn("Error searchin by mask{} in driver {} ", mask, volume.getValue(), e);
        response.addWarning(RsError.SEARCH.generateJsonResponse(e.getMessage()));
      }
    }
    return response;
  }

  /**
   * Catch exception of controller and wrap to map for json format.
   * 
   * @param ex
   *          - catched exception
   * @return error property map
   */
  @ExceptionHandler(RsManagerException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ErrorJsonResponse handleRsManagerExeption(RsManagerException rsex) {
    logger.error("Catching RsManagerException exception with code [{}] , details are [{}] ", 
        rsex.getErrorCode(), rsex.getDetails());
    return new ErrorJsonResponse(rsex.getErrorCode(), rsex.getDetails());
    /*
     * Map<String, String> map = new HashMap<String, String>();
     * map.put("message", ex.getMessage()); map.put("code", ex.getErrorCode() +
     * ""); map.put("type", ex.getErrorType()); return map;
     */
  }

  /**
   * Catch exception of controller and wrap to map for json format.
   * 
   * @param ex
   *          - catched exception
   * @return error property map
   */
  @ExceptionHandler(Exception.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ErrorJsonResponse handleRsManagerExeption(Exception ex) {
    logger.error("Catching unsupported exception", ex);
    return new ErrorJsonResponse(RsError.UNKNOWN.name(), ex.getMessage());
    /*
     * Map<String, String> map = new HashMap<String, String>();
     * map.put("message", ex.getMessage()); map.put("code", ex.getErrorCode() +
     * ""); map.put("type", ex.getErrorType()); return map;
     */
  }
  /**
   * Parse volume alias from input path.
   * @param url - input resource path
   * @return volumeAlias
   */
  public String parseVolumeName(String url) {
    logger.debug("parse volumeAlias name by url [{}]", url);
    String volumeAlais = url;
    if (url.indexOf("\\") > -1) {
      volumeAlais = url.split("\\\\")[0];
    }
    logger.debug("Parsed result volumeAlias name by url [{}] is []", url, volumeAlais);
    return volumeAlais;
  }
}
