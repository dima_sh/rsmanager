package ru.sbtsbol.admin.rsmanager.filter;

import java.io.InputStream;

/**
 * Filter interface to check and allow user resource manager action.
 * 
 * @author SBT-Shiryaev-DV
 */
public interface Filter {
  /**
   * Check that resource is readable. (Most strict policy)
   * 
   * @param url - resource path
   * @return <code>true</code> - readable, otherwise not readable
   */
  boolean isReadable(String url);

  /**
   * Check that resource is writable. (Middle strict policy)
   * 
   * @param url - resource path
   * @return <code>true</code> - writable, otherwise not writable
   */
  boolean isWriteable(String url);

  /**
   * Check that resource is locked by some process or used somewhere. (most soft
   * policy)
   * 
   * @param url  - resource path
   * @return <code>true</code> - locked, otherwise not locked
   */
  boolean isLocked(String url);

  /**
   * Checking, Can I create here new resource.
   * 
   * @param content  - content's byte of resource
   * @param url      - url of new resource
   * @return <code>true</code> - I can create, otherwise can't
   */
  boolean canCreate(byte[] content, String url);

  /**
   * Checking, Can I create here new resource.
   * 
   * @param content   - content's inputstream of resource
   * @param url       - url of new resource
   * @return <code>true</code> - I can create, otherwise can't
   */
  boolean canCreate(InputStream is, String url);

}
