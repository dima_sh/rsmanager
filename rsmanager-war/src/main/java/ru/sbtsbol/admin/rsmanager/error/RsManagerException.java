package ru.sbtsbol.admin.rsmanager.error;

/**
 * RsManagerException.
 * @author SBT-Shiryaev-DmV
 */
public class RsManagerException extends Exception {
  /** serialVersionUID. */
  private static final long serialVersionUID = 433373874081082219L;
  private String errorCode = RsError.UNKNOWN.name();
  private String details = "";

  public RsManagerException() {
    super();
  }

  public RsManagerException(Throwable cause) {
    super(cause);
    details = cause.getMessage();
  }

  public RsManagerException(String message) {
    super(message);
    details = message;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public String getDetails() {
    return details;
  }

  public void setDetails(String details) {
    this.details = details;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }

}
