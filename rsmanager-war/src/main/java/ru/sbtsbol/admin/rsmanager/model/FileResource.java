package ru.sbtsbol.admin.rsmanager.model;

import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.xerces.impl.dv.util.Base64;

import ru.sbtsbol.admin.rsmanager.util.RsManagerUtil;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

/**
 * Resource implementation on file system.
 * 
 * @author SBT-Shiryaev-DmV
 * 
 */
public class FileResource implements Resource {

	private File file;
	private String rootPath;
	private String rootName;
	private boolean isLocked;
	private boolean isWritable;
	private boolean isReadable;

	/**
	 * Constructor.
	 * 
	 * @param file
	 *            - file instance
	 * @param rootPath
	 *            - root for file resource
	 * @param rootName
	 *            - root alias
	 */
	public FileResource(File file, String rootPath, String rootName) {
		this.file = file;
		this.rootName = rootName;
		this.rootPath = rootPath;
	}

	@Override
	public String getId() {
		return new String(Base64.encode(getAbsolutePath().getBytes()));
	}

	@Override
	public String getRelativePath() {
		String filePath = file.getPath();
		return filePath.substring(rootPath.length());
	}

	@Override
	public String getAbsolutePath() {
		String filePath = file.getPath();
		return rootName + "\\" + filePath.substring(rootPath.length());
	}

	@Override
	public String getMimeTypes() {
		if (file.isDirectory()) {
			return "directory";
		} else {
			return RsManagerUtil.getMimeTypeByFileName(getFile().getName());
		}
	}

	@Override
	public Date getModifiedDate() {
		return new Date(file.lastModified());
	}

	// TODO check security
	@Override
	public Boolean isReadable() {
		return isReadable;
	}

	// TODO check security
	@Override
	public Boolean isWriteable() {
		return isWritable;
	}

	// TODO check buzy
	@Override
	public Boolean isLocked() {
		return isLocked;
	}

	public void setLocked(boolean locked) {
		isLocked = locked;
	}

	public boolean isWritable() {
		return isWritable;
	}

	public void setWritable(boolean isWritable) {
		this.isWritable = isWritable;
	}

	public void setReadable(boolean isReadable) {
		this.isReadable = isReadable;
	}

	@Override
	public String getName() {
		if (isRoot()) {
			return getRootName();
		} else {
			return file.getName();
		}
	}

	@Override
	public Long getSize() {
		return file.length();
	}

	@Override
	public Boolean isRoot() {
		return getFile().getAbsolutePath().length() == rootPath.length();
	}

	@Override
	public boolean isImage() {
		String mime = getMimeTypes();
		String type = mime.split("/")[0];
		if ("image".equals(type)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public InputStream openInputStream() throws IOException {
		return new BufferedInputStream(new FileInputStream(getFile()));
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	@Override
	public Boolean haveDirs() {
		if (file.isDirectory()) {
			String[] subdirs = file.list(FileFilterUtils.directoryFileFilter());
			return subdirs != null;
		}
		return false;
	}

	@Override
	public String getParentPath() {
		if (isRoot()) {
			return "";
		} else {
			String filePath = file.getParentFile().getPath();
			return rootName + "\\" + filePath.substring(rootPath.length());
		}
	}

	public String getRootPath() {
		return rootPath;
	}

	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	}

	public String getRootName() {
		return rootName;
	}

	public void setRootName(String rootName) {
		this.rootName = rootName;
	}

	@Override
	public String toString() {
		return "FileResource [file=" + file + ", rootPath=" + rootPath
				+ ", rootName=" + rootName + ", isLocked=" + isLocked
				+ ", isWritable=" + isWritable + ", isReadable=" + isReadable
				+ ", getId()=" + getId() + ", getRelativePath()="
				+ getRelativePath() + ", getAbsolutePath()="
				+ getAbsolutePath() + ", getMimeTypes()=" + getMimeTypes()
				+ ", getModifiedDate()=" + getModifiedDate()
				+ ", isReadable()=" + isReadable() + ", isWriteable()="
				+ isWriteable() + ", isLocked()=" + isLocked()
				+ ", isWritable()=" + isWritable() + ", getName()=" + getName()
				+ ", getSize()=" + getSize() + ", isRoot()=" + isRoot()
				+ ", isImage()=" + isImage() + ", haveDirs()=" + haveDirs()
				+ ", getParentPath()=" + getParentPath() + ", getRootPath()="
				+ getRootPath() + ", getRootName()=" + getRootName() + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (getClass() != obj.getClass())
			return false;
		FileResource other = (FileResource) obj;
		return other.getId().equals(getId());
	}

}
