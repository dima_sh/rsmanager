package ru.sbtsbol.admin.rsmanager.controller;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import ru.sbtsbol.admin.rsmanager.error.RsError;
import ru.sbtsbol.admin.rsmanager.error.RsManagerException;
import ru.sbtsbol.admin.rsmanager.model.JsonResponse;
import ru.sbtsbol.admin.rsmanager.model.Resource;
import ru.sbtsbol.admin.rsmanager.model.Volume;
import ru.sbtsbol.admin.rsmanager.service.ImageService;
import ru.sbtsbol.admin.rsmanager.service.ResourceManager;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The Main Controller processong Resource Manager API.
 * 
 * @author SBT-Shiryaev-DmV
 * 
 */
@Controller
public class ResourceManagerController extends MessageResourceProvider {
  /** rename operation label. */
  private static final String RENAME = "rename";
  /** replace operation label. */
  private static final String REPLACE = "cut";
  /** copy operation label. */
  private static final String COPY = "copy";
  /** Logger. */
  private static final Logger logger = LoggerFactory.getLogger(ResourceManagerController.class);

  /**
   * Constructor
   * 
   * @param driverConfiguration
   *          - configuration of volums which are mounted.
   */
  @Autowired
  public ResourceManagerController(DriverConfiguration driverConfiguration) {
    super(driverConfiguration);
  }

  @Autowired
  private ImageService imageService;

  /**
   * <p>
   * Command LIST.
   * </p>
   * (Getting resources from parent)
   * <ul>
   * <li>request format:
   * <code>http://{contextPath}/resource/{volumeId}/{path}</code>
   * <li>request example:
   * <code>http://localhost/resource/l1/Folder1\folder2\someFile</code></li>
   * <li>RequestMethod: GET</li>
   * <li>RequestBody: NO BODY</li>
   * </ul>
   * 
   * @param path - Parent Path
   * @return {@link JsonResponse}
   * @throws RsManagerException - when exception on getting list or searching
   */
  @Cacheable(value = "cAdmin", key = "#volumeId + #path")
  @RequestMapping(value = "/resource/{path:.*}", method = RequestMethod.GET, 
               produces = "application/json;charset=UTF-8")
  @ResponseBody
  public JsonResponse listResources(HttpSession session, @PathVariable("path") String path)
      throws RsManagerException {
    logger.info("Command LIST, path [{}]", path);
    String volumeAlias = parseVolumeName(path);
    ResourceManager service = null;
    String mask = null;
    if (volumeAlias.indexOf("s_") < 0) {
      service = findByVolumeAlias(volumeAlias);
    } else {
      mask = volumeAlias.substring("s_".length());
    }
    if (service != null) {
      JsonResponse response = new JsonResponse();
      List<Resource> list = service.listResources(path);
      response.addResources(list);
      return response;
    } else {
      logger.info("Command LIST, path [{}], is SearchAlias!!!", path);
      return searchResources(mask);
    }

  }

 
  /**
   * Getting tumbnail for image resources.
   * <ul>
   * <li>request format:
   * <code>http://{contextPath}/resource/tmb/{volumeId}/{path}</code>
   * <li>request example:
   * <code>http://localhost/resource/tmb/l1/Folder1\folder2\imageFile.jpg</code>
   * </li>
   * <li>RequestMethod: GET</li>
   * </ul>
   * 
   * @param response
   *          - HttpServletResponse
   * @param path
   *          - resource path
   * @throws IOException
   *           - exception with input stream
   */
  @RequestMapping(value = "/resource/tmb/{path:.*}", method = RequestMethod.GET)
  @ResponseBody
  public void tumbSorce(HttpServletResponse response, @PathVariable("path") String path)
      throws IOException, RsManagerException {
    logger.info("Getting tumbnail for image resource [{}]", path);
    ResourceManager service = findByVolumeAlias(parseVolumeName(path));
    Resource resource = service.findResource(path);
    BufferedImage bufferedImage = imageService.getTumb(path, resource);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ImageIO.write(bufferedImage, "png", baos);
    // response.setHeader("Last-Modified",
    // DateUtils..addDays(Calendar.getInstance().getTime(), 2 *
    // 360).toGMTString());
    // response.setHeader("Expires",
    // DateUtils.addDays(Calendar.getInstance().getTime(), 2 *
    // 360).toGMTString());
    ImageIO.write(bufferedImage, "png", response.getOutputStream());

  }

  /**
   * <b>Command DELETE.</b> (Deleting resource)
   * <ul>
   * <li>request format:
   * <code>http://{contextPath}/resource/{volumeId}/{path}</code>
   * <li>request example:
   * <code>http://localhost/resource/l1/Folder1\folder2\someFile</code></li>
   * <li>RequestMethod: DELETE</li>
   * <li>RequestBody: NO BODY</li>
   * </ul>
   * 
   * @param path
   *          - url deleting resource
   * @return json object of deleted resoucre {@link Resource}
   * @throws RsManagerException
   *           delete error or acces denied
   */
  @RequestMapping(value = "/resource/{path:.*}", method = RequestMethod.DELETE, 
      produces = "application/json;charset=UTF-8")
  @ResponseBody
  public Resource deleteResource(@PathVariable("path") String path) throws RsManagerException {
    logger.info("Command DELETE, path [{}]", path);
    ResourceManager service = findByVolumeAlias(parseVolumeName(path));
    Resource res = service.deleteResource(path);
    return res;
  }

  /**
   * <b>Command UPDATE(RENAME, REPLACE, COPY).</b> (Updating resource)
   * <ul>
   * <li>request format:
   * <code>http://{contextPath}/resource/{volumeId}/{path}</code>
   * <li>request example:
   * <code>http://localhost/resource/l1/Folder1\folder2\someFile.txt</code></li>
   * <li>RequestMethod: PUT</li>
   * <li>RequestBody:
   * <ul>
   * <li>operation RENAME: {newname: <b>targetName</b>, parentPath:
   * <b>parentPath</b> }</li>
   * <li>operation REPLACE:{operation: <b>cut</b>, newPath: <b>targetPath</b>,
   * name: <b>filename</b>, newVolumeId: <b>targetVolumeId</b>}</li>
   * <li>operation COPY: {operation: <b>copy</b>, newPath: <b>targetPath</b>,
   * name: <b>filename</b>, newVolumeId: <b>targetVolumeId</b>}</li>
   * </ul>
   * </li>
   * </ul>
   * 
   * @param path
   *          - url deleting resource
   * @return json object of updated resourse {@link Resource}
   * @throws RsManagerException
   *           update error or acces denied
   */
  @Caching(evict = { @CacheEvict(value = "cAdmin", key = "#path"),
      @CacheEvict(value = "cAdmin", key = "80 + #path") })
  @RequestMapping(value = "/resource/{path:.*}", method = RequestMethod.PUT, 
                                produces = "application/json;charset=UTF-8")
  @ResponseBody
  public Resource updateResource(@PathVariable("path") String path,
      @RequestBody Map<String, Object> resource) throws RsManagerException {
    
    if (logger.isDebugEnabled()) {
      String params = "";
      for (Map.Entry<String, Object> set: resource.entrySet()) {
        params += set.getKey() + ":[" + set.getValue() + "];";  
      }
      logger.debug("Command UPDATE, path [{}] , params", path, params);
    } else {
      logger.info("Command UPDATE, path [{}]", path);  
    }
    String operation = null;
    ResourceManager sourceService = findByVolumeAlias(parseVolumeName(path));

    if (StringUtils.hasLength((String) resource.get("newname"))) {
      operation = RENAME;
    } else {
      operation = (String) resource.get("operation");
    }
    logger.info("Command UPDATE, path [{}], operation is [{}]", operation);
    if (operation == null) {
      throw RsError.UPDATEBADREQUEST
          .generateRsException("Can't find out operation type (rename, replace or copy)");
    }

    if (operation.equals(RENAME)) {
      return sourceService.renameResource(path, (String) resource.get("newname"));
    } else if (operation.equals(REPLACE) || operation.equals(COPY)) {
      boolean mustToDelete = operation.equals(REPLACE);
      String newParentPath = (String) resource.get("newParent");
      ResourceManager targetService = findByVolumeAlias((parseVolumeName(newParentPath)));
      if (targetService.equals(sourceService)) {
        return sourceService.replaceResource(path, newParentPath, mustToDelete);
      } else {
        return replace(sourceService, path, targetService, newParentPath, mustToDelete);
      }
     } else {
      throw RsError.UNSUPPORTEDOPERATION.generateRsException(
          String.format("Unkown operation", operation));
    }

  }

  /**
   * <b>Command CREATE.</b> (Creating resource in Parent dir)
   * <ul>
   * <li>request format: <code>(http://{contextPath}/resource)</code>
   * <li>request example: <code>(http://localhost/resource)</code></li>
   * <li>RequestMethod: POST</li>
   * <li>RequestBody (json format): {volumeId: <b>targetVolumeId</b>, mimeTypes:
   * <b>createdMimeType</b>, parent: <b>targetParentPath</b>, name:
   * <b>createdResourceName</b>}</li>
   * </ul>
   * 
   * @param resource
   *          see: RequestBody
   * @return json object of created Resource {@link Resource}
   * @throws RsManagerException
   *           - created error or acces denied
   */
  // TODO rename mime types to mimeType
  @RequestMapping(value = "/resource", method = RequestMethod.POST, 
       produces = "application/json;charset=UTF-8")
  @ResponseBody
  public Resource createResource(@RequestBody Map<String, Object> resource)
      throws RsManagerException {
    if (logger.isDebugEnabled()) {
      String params = "";
      for (Map.Entry<String, Object> set: resource.entrySet()) {
        params += set.getKey() + ":[" + set.getValue() + "];";  
      }
      logger.debug("Command CREATE, , params",  params);
    } else {
      logger.info("Command CREATE");  
    }
    ResourceManager service = findByVolumeId((String) resource.get("volumeId"));
    if (Resource.DIRECTORY_MIME.equals((String) resource.get("mimeTypes"))) {
      String normalizedParent = ((String) resource.get("parent")).replaceAll("%5C", "\\\\");
      return service.mkDir(normalizedParent + "\\" + (String) resource.get("name"));
    } else {
      throw new UnsupportedOperationException("create file is not implemented");
    }

  }

  /**
   * <b>Command VOLUMES</b> (Getting volume drivers which are mounted. This
   * method calls at first)<br>
   * <ul>
   * <li>request format: <code>(http://{contextPath}/resource/volume)</code>
   * <li>request example: <code>(http://localhost/resource/volume)</code></li>
   * <li>request method: GET</li>
   * <li>params: No params</li>
   * </ul>
   * 
   * @return json list with objects {@link Volume}
   * @throws IOException
   */
  // TODO change IO exception to RSMAnager Exception
  @RequestMapping(value = "/resource/volume", method = RequestMethod.GET, 
      produces = "application/json;charset=UTF-8")
  @ResponseBody
  public List<Volume> listDrivers() throws IOException {
    logger.info("Command VOLUMES");  
    List<Volume> drivers = new ArrayList<Volume>();
    for (Map.Entry<String, ResourceManager> volume : getDrivers().entrySet()) {
      drivers.add(new Volume(volume.getKey(), volume.getValue().getRootName()));
    }
    return drivers;
  }

  /**
   * <b>Command UPLOAD.</b> (Upload multiple file using Spring Controller)<br>
   * <ul>
   * <li>request format:
   * <code>(http://{contextPath}/resource/upload/{volume})</code>
   * <li>request example: <code>(http://localhost/resource/upload/l1)</code>
   * <li>request method: POST</li>
   * <li>request body: name=upload[]</li>
   * </ul>
   */
  // TODO must return error file
  @RequestMapping(value = "/resource/upload/{path:.*}", method = RequestMethod.POST, 
      produces = "application/json;charset=UTF-8")
  @ResponseBody
  public List<Resource> uploadMultipleFileHandler(@RequestParam("upload[]") MultipartFile[] files,
      @PathVariable("path") String path) throws RsManagerException {
    logger.info("Command UPLOAD to path [{}]", path); 
    List<Resource> resources = new ArrayList<Resource>();
    ResourceManager service = findByVolumeAlias(parseVolumeName(path));
    logger.info("Command UPLOAD to path [{}]: uploaded [{}] files", path,  files.length); 
    for (int i = 0; i < files.length; i++) {
      MultipartFile file = files[i];
      try {
        byte[] bytes = file.getBytes();
        logger.debug("Command UPLOAD to path [{}]: creating [{}] file with size [{}]", path,
            file.getOriginalFilename(), bytes.length); 
        Resource resource = service.createFile(bytes, path + "\\" + file.getOriginalFilename());
        resources.add(resource);
      } catch (Exception e) {
        //TODO set to warn
        e.printStackTrace();
      }
    }
    return resources;

  }

  /**
   * <b>Command DOWNLOAD.</b> (Download file)
   * <ul>
   * <li>request format:
   * <code>(http://{contextPath}/resources/get/{volume})</code>
   * <li>request example: <code>(http://localhost/resources/get/l1)</code>
   * <li>request method: GET</li>
   * <li>request body: name=upload[]</li>
   * </ul>
   */
  @RequestMapping(value = "/resource/get/{volumeId}/{path:.*}", method = RequestMethod.GET)
  @ResponseBody
  private void download(HttpServletRequest request, HttpServletResponse response,
      @PathVariable("volumeId") String volumeId, @PathVariable("path") String path)
      throws IOException, RsManagerException {
    downloadNew(request, response, path);
  }

  /**
   * <b>Command DOWNLOAD.</b> (Download file)
   * <ul>
   * <li>request format:
   * <code>(http://{contextPath}/resources/get/{volume})</code>
   * <li>request example: <code>(http://localhost/resources/get/l1)</code>
   * <li>request method: GET</li>
   * <li>request body: name=upload[]</li>
   * </ul>
   */
  @RequestMapping(value = "/resource/get/{path:.*}", method = RequestMethod.GET)
  @ResponseBody
  private void downloadNew(HttpServletRequest request, HttpServletResponse response,
      @PathVariable("path") String path) throws IOException, RsManagerException {
    logger.info("Command DOWNLOAD [{}]", path); 
    ResourceManager service = findByVolumeAlias(parseVolumeName(path));
    Resource resource = service.findResource(path);
    String mime = resource.getMimeTypes();
    response.setCharacterEncoding("utf-8");

    response.setContentType(mime);
    // String fileName = resource.getName();
    response.setHeader("Content-Transfer-Encoding", "binary");
    /*
     * if (downLoadFlag) { response.setHeader("Content-Disposition",
     * "attachments; " + getAttachementFileName(fileName,
     * request.getHeader("USER-AGENT")));
     * //response.setHeader("Content-Location", fileUrlRelative);
     * response.setHeader("Content-Transfer-Encoding", "binary"); }
     */
    OutputStream out = response.getOutputStream();
    InputStream is = null;
    response.setContentLength(new Integer(resource.getSize() + ""));
    try {
      // serve file
      is = service.getInputStream(resource.getAbsolutePath());
      IOUtils.copy(is, out);
      out.flush();
      out.close();
    } finally {
      IOUtils.closeQuietly(is);
    }
  }

}
