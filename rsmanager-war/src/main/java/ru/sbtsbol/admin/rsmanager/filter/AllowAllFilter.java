package ru.sbtsbol.admin.rsmanager.filter;

import java.io.InputStream;

/**
 * AllowAllAction filter stub.
 * @author SBT-Shiryaev-DV
 */
public class AllowAllFilter extends DenyAllFilter implements Filter {

  @Override
  public boolean isReadable(String url) {
    return true;
  }

  @Override
  public boolean isWriteable(String url) {
    return true; 
  }

  @Override
  public boolean canCreate(byte[] content, String url) {
    return true;
  }

  @Override
  public boolean canCreate(InputStream is, String url) {
    return false; 
  }
}
