package ru.sbtsbol.admin.rsmanager.model;

/**
 * Driver bean of volume.
 * 
 * @author SBT-Shiryaev-DmV
 * 
 */
public class Volume {

  public static final String SEARCH_TYPE = "search";
  public static final String BUCKET_TYPE = "bucket";
  public static final String FAVORITE_TYPE = "favorite";
  public static final String CORE_TYPE = "core";

  /** Driver Id. */
  private String id;
  /** Driver Root name. */
  private String name;
  /** Type of volume (CORE,SEARCH,BUCKET,FAVORITE). */
  private String type;

  /**
   * Constructor
   * 
   * @param id
   *          - id of driver volume
   * @param name
   *          - rootName of driver volume.
   */
  public Volume(String id, String name) {
    super();
    this.id = id;
    this.name = name;
    this.type = CORE_TYPE;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String volumeType) {
    this.type = volumeType;
  }

}
