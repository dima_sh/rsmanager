package ru.sbtsbol.admin.rsmanager.service;

import ru.sbtsbol.admin.rsmanager.model.Resource;
import ru.sbtsbol.admin.rsmanager.util.RsManagerUtil;
import net.sf.image4j.codec.ico.ICODecoder;

import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;




import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.imageio.ImageIO;

/**
 * Created with IntelliJ IDEA. 
 * User: SBT-Konovalov-EI Date: 21.05.15 Time: 13:20
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ImageServiceImpl implements ImageService {

  private static final Logger logger = LoggerFactory.getLogger(ImageServiceImpl.class);
  @Override
  @Cacheable(value = "cAdmin", key = "80 + #path")
  public BufferedImage getTumb(String path, Resource resource) throws IOException {
    logger.info("Tumbling image url by path [{}] ", path);
    InputStream is = resource.openInputStream();
    try {
      String mime = RsManagerUtil.getMimeTypeByFileName(resource.getName());
      if (!mime.contains("image")) {
        throw new IOException("Not image");
      } else {
        BufferedImage image = null;
        if (mime.contains("svg")) {
          byte[] png = svgToPng(is);
          is = new BufferedInputStream(new ByteArrayInputStream(png));
          image = ImageIO.read(is);
        } else if (mime.contains("ico")) {
          image = icoToPng(is);
        } else {
          image = ImageIO.read(is);
        }
        BufferedImage bufferedImage = Scalr.resize(image, 80);
        return bufferedImage;
      }
    } finally {
      is.close();
    }
  }

  /**
   * Convert svg format to png format.
   * 
   * @param is
   *          inputStream with svg data
   * @return binary png
   * @throws IOException
   *           on problem/
   */
  private static byte[] svgToPng(InputStream is) throws IOException {
    TranscoderInput inputSvgImage = new TranscoderInput(is);
    OutputStream pngOstream = new ByteArrayOutputStream();
    TranscoderOutput outputImage = new TranscoderOutput(pngOstream);
    PNGTranscoder converter = new PNGTranscoder();
    try {
      converter.transcode(inputSvgImage, outputImage);
    } catch (Exception e) {
      throw new RuntimeException(e);
    } finally {
      pngOstream.flush();
      pngOstream.close();

    }
    return ((ByteArrayOutputStream) pngOstream).toByteArray();
  }

  private static BufferedImage icoToPng(InputStream is) throws IOException {
    List<BufferedImage> imgList = ICODecoder.read(is);
    return imgList.get(0);
  }

}
