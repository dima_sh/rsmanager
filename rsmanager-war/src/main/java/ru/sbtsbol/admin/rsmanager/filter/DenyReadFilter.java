package ru.sbtsbol.admin.rsmanager.filter;

/**
 * DenyReadFilterStub.
 * 
 * @author SBT-Shiryaev-DV
 */
public class DenyReadFilter extends AllowAllFilter implements Filter {

  @Override
  public boolean isReadable(String url) {
    return false;
  }

}
