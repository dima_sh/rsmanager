package ru.sbtsbol.admin.rsmanager.filter;

import java.io.InputStream;

/**
 * DenyAllFilter Stub.
 * @author SBT-Shiryaev-DV
 */
public class DenyAllFilter implements Filter {

  public boolean isLocked(String url) {
    return false;
  }

  @Override
  public boolean isReadable(String url) {
    return false;
  }

  @Override
  public boolean isWriteable(String url) {
    return false;
  }

  @Override
  public boolean canCreate(byte[] content, String url) {
    return false;
  }

  @Override
  public boolean canCreate(InputStream is, String url) {
    return false;
  }
}
