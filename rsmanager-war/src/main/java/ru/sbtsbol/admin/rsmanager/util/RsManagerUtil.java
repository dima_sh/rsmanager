package ru.sbtsbol.admin.rsmanager.util;

import org.apache.tika.Tika;


import java.io.IOException;
import java.io.InputStream;

/**
 * Util class for RSManager.
 * 
 * @author SBT-Shiryaev-DmV
 * 
 */
public class RsManagerUtil {
  /** unknown mime type label.  */
  public static String UNKNOWN_MIME = "application/oct-stream".intern();
  
  private static Tika tika;

  static {
    tika = new Tika();
  }

  public static String getMimeTypeByFileName(String name) {
    return tika.detect(name);
  }
  /**
   * Getting mime type for inputStraem
   * @param is {@link InputStream} of object
   * @return - Mime type (if not detected mimetype, then return RsManagerUtil.UNKNOWN_MIME)
   */
  public static String getMimeTypeByContent(InputStream is) {
    try {
      return tika.detect(is);
    } catch (IOException e) {
      return UNKNOWN_MIME;
    }
  }

}
