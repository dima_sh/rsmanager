package ru.sbtsbol.admin.rsmanager.controller;

import ru.sbtsbol.admin.rsmanager.service.ResourceManager;

import java.util.Map;

/**
 * Settings driver mapping to show on client.
 * 
 * @author SBT-Shiryaev-DmV
 * 
 */
public class DriverConfiguration {

  private String defaultDriver;
  private Map<String, ResourceManager> drivers;

  public String getDefaultDriver() {
    return defaultDriver;
  }

  public void setDefaultDriver(String defaultDriver) {
    this.defaultDriver = defaultDriver;
  }

  public Map<String, ResourceManager> getDrivers() {
    return drivers;
  }

  public void setDrivers(Map<String, ResourceManager> drivers) {
    this.drivers = drivers;
  }

}
