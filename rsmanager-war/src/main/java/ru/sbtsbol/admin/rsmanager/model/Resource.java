package ru.sbtsbol.admin.rsmanager.model;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

/**
 * Resource interface for resource manager.
 * 
 * @author SBT-Shiryaev-DmV
 * 
 */
public interface Resource {
  /** directory mime type label. */
  public static String DIRECTORY_MIME = "directory";

  /**
   * @return unique resource id.
   */
  String getId();

  /**
   * @return Absolute resource path (with root name).
   */
  String getAbsolutePath();

  /**
   * @return Relative resource path (without root name).
   */
  String getRelativePath();

  /**
   * @return mime type of object.
   */
  String getMimeTypes();

  /**
   * @return Last modification date of resource/.
   */
  Date getModifiedDate();

  /**
   * @return Readable flag of resource. The <code>false</code> means no
   *         permission to see or download content (haviest permission).
   */
  Boolean isReadable();

  /**
   * @return Writable flag of resource. The <code>false</code> means no
   *         permission to edit or overwrite one (havier permission).
   */
  Boolean isWriteable();

  /**
   * @return Lock flag. The <code>true</code> means no permission to replace or
   *         delete one. (soft permission).
   */
  Boolean isLocked();

  /**
   * @return resource name.
   */
  String getName();

  /**
   * @return Size of resource.
   */
  Long getSize();

  /**
   * @return <code>true</code> means that is the root resource.
   */
  Boolean isRoot();

  /**
   * Resource hase subdir condition.
   * 
   * @return <code>true</code> means that has subdirs
   */
  Boolean haveDirs();

  /**
   * @return return parentPath resource.
   */
  String getParentPath();

  /**
   * Image flag.
   * 
   * @return <code>true</code> means this resource is image/
   */
  boolean isImage();

  @Deprecated
  // TODO Konovalov. Remove this method in service
  /**
   * Get input stream of resource body.
   * @return - input stream of body
   * @throws IOException
   */
  InputStream openInputStream() throws IOException;

}
