package ru.sbtsbol.admin.rsmanager.service;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.sbtsbol.admin.rsmanager.error.RsManagerException;
import ru.sbtsbol.admin.rsmanager.filter.Filter;
import ru.sbtsbol.admin.rsmanager.filter.FilterException;
import ru.sbtsbol.admin.rsmanager.model.FileResource;
import ru.sbtsbol.admin.rsmanager.model.Resource;



import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Some base and abstract method for file resource implemetation.
 * @author SBT-Shiryaev-DV
 *
 */
public abstract class AbstractFileResourceManager implements ResourceManager {
  
  private static final Logger logger = LoggerFactory.getLogger(AbstractFileResourceManager.class);

  protected String rootPath;
  protected String rootName;
  protected Filter filterManager;

  public String getRootPath() {
    return rootPath;
  }

  public void setRootPath(String rootPath) {
    this.rootPath = rootPath;
  }

  public String getRootName() {
    return rootName;
  }

  public void setRootName(String rootName) {
    this.rootName = rootName;
  }

  public Filter getFilterManager() {
    return filterManager;
  }

  public void setFilterManager(Filter filterManager) {
    this.filterManager = filterManager;
  }

  public Resource findResource(String url) throws RsManagerException {
    return getResource(url);
  }

  protected FileResource getResource(String url) {
    logger.info("Getting resource [{}] ", url);
    String path = rootPath;
    if (url != null && !"".equals(url)) {
      path = path + url.substring((rootName).length());
    }
    logger.info("Mapping resource to path [{}] ", path);
    File file = new File(path);
    FileResource resource = new FileResource(file, rootPath, rootName);
    setSecutiryField(resource);
    logger.debug("Returning Resource [{}]", resource.toString());;
    return resource;
  }

  protected void createDir(FileResource dir) throws FilterException, IOException {
    checkUpdate(dir);
    if (!dir.getFile().isDirectory() && dir.getFile().exists()) {
      throw new IOException("resource.error.direxist");

    }
    dir.getFile().mkdir();

  }

  protected void copyFile(FileResource source, FileResource target) throws FilterException,
      IOException {
    checkUpdate(source);
    createFile(getInputStream(source), target);
  }

  protected void deleteDirRecoursively(FileResource resource) throws FilterException, IOException {
    logger.info("Deleteng dir [{}] recoursively", resource.getAbsolutePath());
    checkRemove(resource);
    if (!resource.getFile().isDirectory()) {
      throw new IOException("resource is not directory");
    }
    List<FileResource> subfiles = listResources(resource);
    for (FileResource subfile : subfiles) {
      try {
        if (subfile.getFile().isDirectory()) {
          deleteDirRecoursively(subfile);
        } else {
          deleteFile(subfile);
        }
      } catch (Exception exc) {
        logger.error("can't delete resource", exc);
      }
    }
    deleteFile(resource);

  }

  protected void deleteFile(FileResource resource) throws FilterException, IOException {
    logger.info("Deleteng file [{}] recoursively", resource.getAbsolutePath());
    checkRemove(resource);
    boolean result = resource.getFile().delete();
    if (!result) {
      throw new IOException(String.format("Can't delete file [%s]", resource.getAbsolutePath()));
    }
  }

  protected void copyDirectoryRecoursively(FileResource source, FileResource target)
      throws IOException, FilterException {
    logger.info("Copy dir from [{}] to [{}] recoursively", source.getAbsolutePath(),
        target.getAbsolutePath());
    checkDownload(source);
    File sourceFile = source.getFile();
    if (!sourceFile.isDirectory()) {
      throw new IOException("!isDirectory");
    }
    createDir(target);
    List<FileResource> subfiles = listResources(source);
    for (FileResource subfile : subfiles) {
      String subPath = subfile.getAbsolutePath().substring(source.getAbsolutePath().length());
      String subtargetpath = target.getAbsolutePath() + subPath;
      FileResource subtarget = getResource(subtargetpath);
      try {
        if (subfile.getFile().isDirectory()) {
          copyDirectoryRecoursively(subfile, subtarget);
        } else if (!subtarget.getFile().exists()) {
          copyFile(subfile, subtarget);
        } else {
          logger.warn("Can't copy from [{}] to [{}]. File exist", subfile.getAbsolutePath(), 
              subtarget.getAbsolutePath());
        }
      } catch (Exception e) {
        logger.error("Error subcopy",e);
      }
    }
  }
  
  /**
   * Get list of resources. 
   * @param resource - parent file resource {@link FileResource}}
   * @return collection of fileResources.
   */
  public List<FileResource> listResources(FileResource resource) {
    String realPath = rootPath + "\\"
        + resource.getAbsolutePath().substring((rootName + "\\").length());
    File root = new File(realPath);
    File[] files = root.listFiles();
    List<FileResource> resources = new ArrayList<FileResource>();
    if (files != null) {
      for (File file : files) {
        FileResource res = new FileResource(file, rootPath, rootName);
        setSecutiryField(res);
        resources.add(res);
      }
    }
    return resources;
  }
  /**
   * create file operation.
   * @param is - inputStream of content
   * @param target - target file where write content.
   * @throws IOException - when write error
   * @throws FilterException - operation deny error
   */
  protected void createFile(InputStream is, FileResource target) 
      throws IOException, FilterException {
    FileOutputStream os = null;
    FileResource parent = getResource(target.getParentPath());
    checkUpdate(parent);
    try {
      os = new FileOutputStream(target.getFile());
      IOUtils.copyLarge(is, os);
    } finally {
      IOUtils.closeQuietly(os);
      IOUtils.closeQuietly(is);
    }
  }

  protected InputStream getInputStream(Resource resource) throws IOException, FilterException {
    checkDownload(resource);
    return new BufferedInputStream(new FileInputStream(((FileResource) resource).getFile()));
  }

  protected void setSecutiryField(FileResource resource) {
    resource.setLocked(filterManager.isLocked(resource.getAbsolutePath()));
    resource.setReadable(filterManager.isReadable(resource.getAbsolutePath()));
    resource.setWritable(filterManager.isReadable(resource.getAbsolutePath()));
  }

  protected void checkUpdate(Resource resource) throws FilterException {
    if (!resource.isWriteable() && !resource.isReadable()) {
      throw new FilterException("resource.error.checkupdate");
    }
  }

  protected void checkRemove(Resource resource) throws FilterException {
    if (resource.isLocked() && !resource.isWriteable() && !resource.isReadable()) {
      throw new FilterException("resource.error.checkremove");
    }
  }

  protected void checkDownload(Resource resource) throws FilterException {
    if (!resource.isReadable()) {
      throw new FilterException("resource.error.checkread");
    }
  }

}
