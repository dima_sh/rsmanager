Welcome to resource manager project!
================
This project allows you from web client manage your resources (image, binary files, etc). The backend level based on RESTFULL api with Spring mvc technology.
The client level use Backbone MVC framework, that helps to use RESTFULL models agreements and customize UI templates for your cases.



--------

# Brief description

Resource Manager gives you base operations on resource objects, this is navigate, search, download, add(upload), remove, rename or (cut/copy) and paste resource object between directories and mounted volumes.

Below you see how this looks like on web browsers:

![rsmanager example](rsmanager-screenshot.png)

As you see from picture, resource manager looks like common file explorer, but with customizing templates you can change your UI as you want. The roots on left panel are mounted as resource drivers, which implemented order to RestFull api interface described bellow.  

### Backend structure
  
  The project is structured as maven project, all java sources are located in folder *src/main*
  To getting started resource manager need to determine at least one driver. In file you should map the driver as shown on bellow code:
 
    <bean id="fileResourceManagerService" class="ru.sbtsbol.admin.rsmanager.service.FileResourceManagerImpl">
        <property name="rootPath" value="c:/TEMP" />
        <property name="rootName" value="ImgResources" />
        <property name="filterManager" ref="filterManager" />
    </bean>
    
 The *rootPath* property describes the root path for file resource driver.  
 The *rootName* describes driver alias, which will shown on navigation menu.   
 The *filterManager* is security filter for resource manager operations.  
  
  To map drivers you should set all drivers in file *spring-resources.xml* as shown on example bellow:  
    
    <util:map id="drivers" key-type="java.lang.String" value-type="ru.sbtsbol.admin.rsmanager.service.ResourceManager" >
       <entry key="l1" value-ref="fileResourceManagerService"/>
       <entry key="l2" value-ref="fileResourceManagerService1"/>
       <entry key="t1" value-ref="fileResourceManagerService2"/>
       <entry key="t2" value-ref="fileResourceManagerService3"/>
    </util:map>
      
**Important!!!** To enable api on Apache Tomcat sould to set in catalina.properties next settings:

    org.apache.catalina.connector.CoyoteAdapter.ALLOW_BACKSLASH=true  
    org.apache.tomcat.util.buf.UDecoder.ALLOW_ENCODED_SLASH=true
  

### Fronend structrue

The main client js scripts are located as js module defined by require.js in folder js

* /webapp
	* */js/app* - common components.
	* */js/app/modules/rsmanager* - resource manager backbone adaptive structure.
	* */style*  - less files that are normalize and compressed in css in production mode.
	* */libs* - required javascript libs.
	* */templates* - common used templates.
	* */templates/rsmanager* - resource manager templates.



## Model description


###Volume 
  
  Describes object volume

    {
     "id" : "l1",              // Volume id
     "name" : "ImgResources",  // Volume alias
     "type" : "core"           // Volume type (core, bucket - deleted resources, search - founded resources, favorite - favorite resources)
    }

 
 
###Resource
 
     {
      "id" : "SW1nUmVzb3VyY2VzXFxuZXcgZm9sZGVy",
      "relativePath" : "\\new folder",
      "absolutePath" : "ImgResources\\\\new folder",
      "mimeTypes" : "directory",
      "modifiedDate" : "2015-06-25T13:54:29Z",
      "readable" : true,
      "writeable" : true,
      "locked" : false,
      "writable" : true,
      "name" : "new folder",
      "size" : 0,
      "root" : false,
      "image" : false,
      "parentPath" : "ImgResources\\"
     }
  
###Error  

    {
     "code" : "UNKNOWN",     //error code
     "details" "Some Errors" //error details  
    }
    

 
------------------
##Error codes 

|     |     |
|-----|-----|
|UNKNOWN| unknown error |
|DELETE| error while deleting resource |
|CREATE| error while creating object |
|RENAME| error while renaming resource|
|CUTPASTE| error on cut/paste operation| 
|COPYPASTE| error on copy/paste operation|
|UPDATEBADREQUEST| bad or missing params on update operations|
|UNSUPPORTEDOPERATION| Unsupported kind of operation|
|SEARCH| Error while searching|
|LIST| Error getting list of resources|
|GETRESOURCE| Error getting resource|
|DELETEDENY| delete operation doesn't permit|
|CREATDENY| create operation doesn't permit|
|RENAMEEXIST| error while renaming, resource already exists|
|RENAMEDENY| rename restricted, no permission|
|VOLUMEGET| error getting volume by id|
|VOLUMEFIND| error getting volume by alias|
|NODIRCONTENT| error creating resource, target not exists|
|GETCONTENTDENY| error uploading, no permission|
|GETCONTENT| error getting content|





## Server Api
  
###Command VOLUMES. 
  
(Getting volume drivers which are mounted. This method calls at first)

* **request format:**    http://*{contextPath}*/resource/volume
* **request example:**   http://localhost/resource/volume
* **method:**    GET
* **responce:**    {Array of [volume](#markdown-header-volume)}    
   
 
###Command LIST. 
  
(Getting resources from parent)

* **request format:**    http://*{contextPath}*/resource/*{volumeId}*/*{path}*
* **request example:**    http://localhost/resource/volume
* **method:**    GET
* **responce:**    {resources:[Array of [resource](#markdown-header-resource)], warnings:[Array of [error](#markdown-header-error)]}


###Command CREATE.

(Creating resource in Parent directory)

* **request format**: http://*{contextPath}*/resource
* **RequestMethod:** POST
* **RequestBody:** {volumeId: *targetVolumeId*, mimeTypes: *createdMimeType*, parent: *targetParentPath*, name: *createdResourceName* }
* **responce:** if success: [created resource](#markdown-header-resource) otherwise: [error](#markdown-header-error)


###Command UPDATE. 

(Updating resource RENAME, REPLACE, COPY)
   
* **request format:** http://*{contextPath}*/resource/*{volumeId}*/*{path}*
* **request example:** http://localhost/resource/Image Resources/Folder1\folder2\someFile.txt
* **RequestMethod:** PUT
* **RequestBody:** 
    * { operation: *rename*, newname:*targetName*, parentPath: *parentPathName* }
    * { operation: *cut*, newPath: *targetPath*, name: *filename*, newVolumeId: *targetVolumeId* } 
    * { operation *copy*: newPath: *targetPath*, name: *filename*, newVolumeId: *targetVolumeId* }
* **responce:** if success: [created resource](#markdown-header-resource) otherwise: [error](#markdown-header-error) 


###Command DELETE.

(Deleting resource)

* **request format:** http://*{contextPath}*/resource/*{volumeId}*/*{path}*
* **request example:** http://localhost/resource/ImageResources/Folder1\folder2\someFile.txt
* **RequestMethod:** DELETE
* **responce:** if success: [created resource](#markdown-header-resource) otherwise: [error](#markdown-header-error)


###Command Download.

(Download resource)

* **request format:** http://*{contextPath}*/resources/get/*{path}*
* **request example:** http://localhost/resources/get/ImageResources/Folder1\folder2\someFile.txt
* **request method:** GET

 
###Command Upload.

(Upload multiple resources)

* **request format:** /resource/upload/*{path}*
* **request example:** http://localhost/resources/upload/ImageResources/Folder1
* **request method:** POST
* **request body:** upload[]
* **responce:**    {resources:[Array of [resource](#markdown-header-resource)], warnings:[Array of [error](#markdown-header-error)]}


###Tumbnail.

(Utility function to getting tumbnail of image resources)

* **request format:** /resource/tmb/*path*
* **request example:** http://localhost/resources/upload/ImageResources/Folder1\image1.png
* **request method:** GET
* **responce:** tumbnail image in png format


##Wysiwyg Inegration 

See the example bellow how rsmanager module integrate with some  

    define([
       'underscore',
       'backbone',
       'backbone.marionette',
       'app/application',
       'hbs!tmpl/services/serviceform',
       'app/modules/services/views/composite/TbView',
       'app/modules/rsmanager/views/layout/RSModalLayout',
       'tinymce'
      ],
      function(_, Backbone, Marionette,  App, Form, TBView, ImgView,  tinymce) {
         'use strict';
         
         //some code here
         
         /** 
         *  Text edit configuration
         **/
         tinymceConfig: {
            convert_urls: true,
            relative_urls: true,
            remove_script_host: false,
            skin_url: '../js/libs/tinymce/skins/lightgray' //FIXME: to baseUrl of require
         },
        
         initialize : function() {
            this.tinymceConfig.file_browser_callback = _.bind(
               function(field_name, url, type, win) {
                    App.modal.show(new ImgView({setUrl: function(url) {
                    win.document.getElementById(field_name).value=url.replace(/\\/g,'%5C');
               }}));
            }, this);
        },

        //some code here

       });
    });
    
    
And result would be somethink like this:


![rsmanager modal](rsmanager-modal.png)    



